<?php

/*
Template Name: Lideranças
*/

function mostraDadosLider($lider) {
	?>

			<div class="span4 lideres-container">
				<a class="lideres-img" href="<?php echo get_permalink($lider->ID); ?>"><?php echo get_the_post_thumbnail($lider->ID, 'lideres_image'); ?></a>
				<div class="lideres-content">
					<a href="<?php echo get_permalink($lider->ID); ?>"><strong><?php echo $lider->post_title; ?></strong></a>
					<a class="uniao" href="<?php echo get_permalink($lider->ID); ?>"><?php echo get_post_meta($lider->ID, 'iasd_cargo', true); ?></a>
					<ul class="unstyled">
						<?php
						$social_network_email = get_post_meta($lider->ID, 'social-networks_email', true);
						if ( $social_network_email ):
						 ?>
						<li>
							<a class="iasd-icon lideres-email" href="mailto:<?php echo $social_network_email; ?>" rel="tooltip" data-placement="top" data-original-title="<?php echo $social_network_email; ?>"></a>
						</li>
						<?php
						endif;
						$social_network_twitter = get_post_meta($lider->ID, 'social-networks_twitter', true);
						if ( $social_network_twitter ):
						 ?>
						<li>
							<a class="iasd-icon lideres-twitter" href="http://www.twitter.com/<?php echo $social_network_twitter; ?>" rel="tooltip" data-placement="top" data-original-title="$social_network_twitter; ?>"></a>
						</li>
						<?php
						endif;
						$social_network_facebook = get_post_meta($lider->ID, 'social-networks_facebook', true);
						if ( $social_network_facebook ):
						 ?>
						<li>
							<a class="iasd-icon lideres-facebook-15" href="http://www.facebook.com/<?php echo $social_network_facebook; ?>" rel="tooltip" data-placement="top" data-original-title="<?php echo $social_network_facebook; ?>"></a>
						</li>
					<?php endif; ?>
					</ul>
					<a class="saber-mais" href="<?php echo get_permalink($lider->ID); ?>"><?php _e('Saiba mais', 'thema_deptos');?></a>
				</div>
			</div>
	<?php
	return true;
}

get_header(); ?>

<section class="departamentos-liderancas">

	<div class="container">

		<?php 
			if(function_exists('breadcrumber')) breadcrumber();

			if (! is_post_type_archive( 'lideres' )){
				the_post();
				the_title('<h1>', '</h1>');
				the_content();
			}

			$lider_geral_query = new WP_Query(array('meta_key' => 'iasd_lider', 'meta_value' => 'on', 'post_type' => 'lideres'));
			$lider_geral = (count($lider_geral_query->posts)) ? reset($lider_geral_query->posts) : null;
			unset($lider_geral_query);

			$lideres_query = new WP_Query(array('post__not_in' => array(($lider_geral) ? $lider_geral->ID : null), 'post_type' => 'lideres', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'iasd_cargo', 'order' => 'ASC'));
			$lideres = (count($lideres_query->posts)) ? $lideres_query->posts : array();
			unset($lideres_query);

			if($lider_geral):
				global $post;
				$post = $lider_geral;
				setup_postdata($post);

?>

		<div id="lider-sul-americana" class="row-fluid">
			<h3><?php echo get_post_meta($lider_geral->ID, 'iasd_cargo', true); ?></h3>
			<a class="span4 lideres-img" href="<?php echo get_permalink($lider_geral->ID); ?>"><?php echo get_the_post_thumbnail($lider_geral->ID, 'thumb_300x300'); ?></a>
			<div class="span7 lideres-content">
				<a href="<?php echo get_permalink($lider_geral->ID); ?>"><strong><?php echo $lider_geral->post_title; ?></strong></a>
				<?php echo $lider_geral->post_content; ?>
				<ul class="unstyled">
					<?php if($email = get_post_meta($lider_geral->ID, 'social-networks_email', true)): ?>
						<li>
							<a href="mailto:<?php echo $email; ?>">
								<span class="iasd-icon lideres-email"></span>
								<?php echo $email; ?>
							</a>
						</li>
					<?php endif; ?>
					<?php if($twitter = get_post_meta($lider_geral->ID, 'social-networks_twitter', true)): ?>
						<li>
							<a href="http://www.twitter.com/<?php echo $twitter; ?>">
								<span class="iasd-icon lideres-twitter"></span>
								<?php echo $twitter; ?>
							</a>
						</li>
					<?php endif; ?>
					<?php if($facebook = get_post_meta($lider_geral->ID, 'social-networks_facebook', true)): ?>
						<li>
							<a href="http://www.facebook.com/<?php echo $facebook; ?>">
								<span class="iasd-icon lideres-facebook-17"></span>
								<?php echo $facebook; ?>
							</a>
						</li>
					<?php endif; ?>
				</ul>
				<a class="saber-mais" href="<?php echo get_permalink($lider_geral->ID); ?>"><?php _e('Saiba mais', 'thema_deptos');?></a>
			</div>
		</div>
<?php
	endif;

	$count = 0;
	foreach($lideres as $k => $lider):
		$count++;
		if($count == 1) {
?>
		<div class="row lideres">
<?php
		if($k == 0) {
?>
			<h3><?php _e('Demais Líderes', 'thema_deptos');?></h3>
<?php

		}
	}

	mostraDadosLider($lider);

	if($count == 3) {
		$count = 0;
?>
		</div>
<?php
	}
	endforeach;
?>
	</div>
</section>

<?php get_footer(); ?>


