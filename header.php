<!DOCTYPE html>
<html>
	<head>
		<!-- Typography -->
		<link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
		<link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
		<link href="<?php echo get_template_directory_uri() ?>/static/lib/fonts/stylesheet.css" rel="stylesheet" type="text/css">
		
		<!-- Stylesheets -->
		<link href="<?php echo get_stylesheet_uri() ?>" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/print.css" type="text/css" media="print" />
		<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/ie8.css" />
		<![endif]-->

		<!-- Meta tags -->
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="description" content="<?php bloginfo('description'); ?>" />
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>
			<?php global $page, $paged;
			wp_title( '|', true, 'right' );
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) )
				echo " | $site_description";
			if ( $paged >= 2 || $page >= 2 )
				echo ' | ' . sprintf( __( 'Página %s', 'pa-thema-institucional' ), max( $paged, $page ) );
			?>
		</title>

		<!--[if IE]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(WPLANG); ?>>
		<?php if(class_exists('PAMenu')) PAMenu::Show('master_header', 'h-departamentos'); ?>
		<header>
			<?php if(class_exists('IasdHeader')) IasdHeader::Show(); ?>
		</header>