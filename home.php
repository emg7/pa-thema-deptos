<?php
/*
Template Name: Home
*/
 get_header(); 
 ?>

<section id="departamentos-index">
	<div class="container">
		<div class="row">
			<div class="span8" sidebar="home-row-1-left">
				<?php dynamic_sidebar('home-row-1-left'); ?>
			</div>
			<div class="span4" sidebar="home-row-1-right">
				<?php dynamic_sidebar('home-row-1-right'); ?>
			</div>
		</div>
		<div class="row">
			<div class="span8" sidebar="home-row-2-left">
				<?php dynamic_sidebar('home-row-2-left'); ?>
			</div>
			<div class="span4" sidebar="home-row-2-right">
				<?php dynamic_sidebar('home-row-2-right'); ?>
			</div>
		</div>
		<div class="row">
			<div class="span4" sidebar="home-row-3-left">
				<?php dynamic_sidebar('home-row-3-left'); ?>
				<!--h2>Eventos</h2>
				<div class="departamentos-eventos">
				  	<ul class="unstyled">
				  		<li>
				  			<span><b>22/09</b><p>14h-16h</p></span>
				  			<a href="#"><b>Treinamento de pastores</b><br/>IASD São Paulo central</a>
				  		</li>
				  		<li>
				  			<span><b>22/09</b><p>14h-16h</p></span>
				  			<a href="#"><b>Reunião ministério jovem</b><br/>IASD Av. Paulista</a>
				  		</li>
				  		<li>
				  			<span><b>22/09</b><p>14h-16h</p></span>
				  			<a href="#"><b>Festa junina desbravadores</b><br/>Rua 7 de setembro, 854</a>
				  		</li>
				  		<li>
				  			<span><b>22/09</b><p>14h-16h</p></span>
				  			<a href="#"><b>Treinamento de pastores</b><br/>IASD Brasília central</a>
				  		</li>
				  		<li>
				  			<span><b>22/09</b><p>14h-16h</p></span>
				  			<a href="#"><b>Envontro de jovens adventistas</b><br/>IASD São Paulo central</a>
				  		</li>
				  		<li>
				  			<span><b>22/09</b><p>14h-16h</p></span>
				  			<a href="#"><b>Envontro de jovens adventistas</b><br/>IASD São Paulo central</a>
				  		</li>
				  	</ul>
				  	<a class="see-more" href="#">Veja mais eventos</a>
				</div-->
			</div>
			<div class="span4" sidebar="home-row-3-middle">
				<?php dynamic_sidebar('home-row-3-middle'); ?>
			</div>
			<div class="span4" sidebar="home-row-3-right">
				<?php dynamic_sidebar('home-row-3-right'); ?>
			</div>
		</div>
		<div class="row">
			<div class="span4" sidebar="home-row-4-left">
				<?php dynamic_sidebar('home-row-4-left'); ?>
			</div>
			<div class="span4" sidebar="home-row-4-middle">
				<?php dynamic_sidebar('home-row-4-middle'); ?>
			</div>
			<div class="span4" sidebar="home-row-4-right">
				<?php dynamic_sidebar('home-row-4-right'); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>