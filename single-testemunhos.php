<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<section id="single-testemunhos" class="departamentos-single">
	<div class="container">
		<?php if(function_exists('breadcrumber')) breadcrumber(); ?>
		<h1><?php the_title(); ?></h1>
		<?php MasterThemeController::SocialWidgets(get_the_title(), get_permalink()); ?>
		<div class="row">
			<div class="span4">
			<?php if(has_post_thumbnail()){ the_post_thumbnail(); ?>
				<span class="wp-caption-text"><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></span>
			<?php } else { ?>

			<?php } ?>
			<?php dynamic_sidebar( 'sidebar-testemunhos' ); ?>
			</div>
			<div class="span8">
<?php
				$testemunho_video = get_post_meta(get_the_ID(), 'testemunhos_video_share_url_field', true);
				if(has_excerpt())
					echo '<h3>' . get_the_excerpt(  ) . '</h3>';
				the_content();
				if(get_post_format($post) == 'video') echo do_shortcode('[fve]'.$testemunho_video.'[/fve]');
?>

				<div class="comments">
					<?php comments_template(); ?>
				</div>
				<a class="testemunho-control" href="<?php echo get_post_type_archive_link('testemunhos'); ?>">&laquo; <?php _e('Voltar', 'thema_deptos');?></a>
			</div>
		</div>
	</div>
</section>
<?php endwhile; ?>

<?php get_footer(); ?>