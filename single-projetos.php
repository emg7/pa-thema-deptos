<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); global $post; $slug = $post->post_name; ?>
	<section id="single-projetos">
		<div class="container">
			<?php if(function_exists('breadcrumber')) breadcrumber(); ?>
			<div class="row">
				<div class="span4">
					<?php 
						if(has_post_thumbnail())
							the_post_thumbnail( 'thumb_300x170' );
						if(!dynamic_sidebar('projetos-'.$slug.'-sidebar'))
							dynamic_sidebar('projetos-sidebar');
					?>
				</div>
				<div class="span8">
					<div class="projetos-main">
						<h1><?php single_post_title(); ?></h1>

						<p><?php the_content(); ?></p>
						<?php if($hotsite = get_post_meta( get_the_ID(), 'project-options-hotsite', true)): ?><a class="more-button pull-right" href="<?php echo $hotsite; ?>"><?php _e('Saiba mais', 'thema_deptos');?></a><?php endif; ?>
					</div>
					<div id="row1" >
						<?php if(!dynamic_sidebar('projetos-'.$slug.'-row-1')) dynamic_sidebar('projetos-row-1'); ?>
					</div>
					<div id="row2" class="row-fluid">
						<div class="span6">
							<?php if(!dynamic_sidebar('projetos-'.$slug.'-row-2-left')) dynamic_sidebar('projetos-row-2-left'); ?>
						</div>
						<div class="span6">
							<?php if(!dynamic_sidebar('projetos-'.$slug.'-row-2-right')) dynamic_sidebar('projetos-row-2-right'); ?>
						</div>
					</div>
					<div id="row3" class="row-fluid">
						<div class="span6">
							<?php if(!dynamic_sidebar('projetos-'.$slug.'-row-3-left')) dynamic_sidebar('projetos-row-3-left'); ?>
						</div>
						<div class="span6">
							<?php if(!dynamic_sidebar('projetos-'.$slug.'-row-3-right')) dynamic_sidebar('projetos-row-3-right'); ?>
						</div>
					</div>

					<div id="row4" class="row-fluid">
						<?php if(!dynamic_sidebar('projetos-'.$slug.'-row-4')) dynamic_sidebar('projetos-row-4'); ?>
					</div>
					<div id="row5" class="row-fluid">
						<?php if(!dynamic_sidebar('projetos-'.$slug.'-row-5')) dynamic_sidebar('projetos-row-5'); ?>
					</div>
					<div id="row6" class="row-fluid">
						<?php if(!dynamic_sidebar('projetos-'.$slug.'-row-6')) dynamic_sidebar('projetos-row-6'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>