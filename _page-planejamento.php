<?php 
/*
Template Name: Planejamento
*/
get_header(); ?> 

<section id="planejamento">
	<div class="container">
		<?php if(function_exists('breadcrumber')) breadcrumber(); ?>
		<?php the_post(); ?>
		<h2><?php echo the_title(); ?></h2>
		<div class="row">
			<div class="span7">
				<article>
					<?php echo the_content(); ?>
				</article>
			</div>
			<div class="span4 offset1">
				<?php get_sidebar( 'planejamento' ); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>