<?php 
/*
Template Name: Default
*/
get_header(); ?> 

<section class="departamentos-page">
	<div class="container">
		<?php if(function_exists('breadcrumber')) breadcrumber(); ?>
		<?php the_post(); ?>
		<div class="row">
			<div class="span8 news-body">
				<h1><?php single_post_title(); ?></h1>
				<?php  MasterThemeController::SocialWidgets($post_title, $post_link); ?>
				<div class="content"><?php the_content(); ?></div>
			</div>
			<div class="sidebar span4">
				<?php dynamic_sidebar('sidebar-blog-single'); ?>
			</div>
			</div>
		</div>
	</div>
</section>


<?php get_footer(); ?>