<?php

if(!class_exists('MasterThemeController'))
    require_once (dirname(__FILE__) . '/classes/MasterThemeController.class.php');

if(function_exists('breadcrumber')) Breadcrumber::HomeName(get_bloginfo('name'));

/** HOOKS **/
add_theme_support( 'post-thumbnails');

remove_action('wp_head', 'wp_generator'); 

add_filter('iasd-header-inner-menu', array('IasdNavEntreCampos', 'callRenderFunc'));