<?php 

/*
Template Name: Biografias
*/

get_header(); ?> 

<section class="departamentos-biografias">
	<div class="container">
		<div class="row biografia">
			<ul class="breadcrumb">
			  <li><a href="#">Lideranças</a> <span class="divider">&rsaquo;</span></li>
			  <li class="active">Líder da União Central Brasileira</li>
			</ul>
			<div class="span4">
				<img src="http://www.placehold.it/300x300" />
			</div>
			<div class="span8">
				<h2>Pr. Ronaldo Arco</h2>
				<strong>Líder da União Central Brasileira</strong>
				<a href="#">Ver equipe completa da União</a>
				<p>Formou-se em Teologia no UNASP em 1988. Concluiu o mestrado em Teologia em 2007.</p>
				<p>Atuou como capelão do Colégio Adventista de São José dos Campos, SP; na mesma função no colégio de Vila Galvão, SP; pastor distrital da cidade de Suzano, SP; departamental de Jovens da Associação Paulista Leste; departamental de Jovens e Comunicação da Missão Paulista do Vale; e departamental do Ministério Jovem nomeado no final de 2005.</p>
				<p>É casado com Junia Bomfim Arco, enfermeira, e têm três filhos: Felipe, e os gêmeos, Mariana e Leonardo.</p>
				<ul class="unstyled">
					<li><a href="#"><span class="email-lideres"></span>prronaldo.arco@adventistas.org</a></li>
					<li><a href="#"><span class="twitter-lideres"></span>@prronaldoarco</a></li>
					<li><a href="#"><span class="facebook-lider-sul-americana"></span>/PastorRonaldoArco</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>