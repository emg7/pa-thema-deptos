(function($){

	$(document).ready(function (){
		
		//Bootstrap Tabs Component
		$('.iasd-global_navbar-more-tabs a').click(function (e) {
			e.preventDefault();
			$(this).tab('show');
		})

		globalNav.init();

	});

	var globalNav = {
		init : function(){
			this.handleEvents();
		},
		
		handleEvents : function(){

			$('.open-more-link').bind('click', globalNav.openMoreMenu);
			$('.close-more-link').bind('click', globalNav.closeMoreMenu);
			$('.btn-navbar').bind('click', globalNav.openResponsiveMenu);
			$('.iasd-global_navbar-more-tabs > li:not(".hidden-desktop") > a').bind('click', globalNav.openTabsMenu);
			$('.back-link').bind('click', globalNav.closeTabsMenu);
			$('.search-link').bind('click', globalNav.openSearchForm);
			$('.institutions .open-associations-link').bind('click', globalNav.openInstitucionsList);
			$('.institutions .map-link').bind('mouseover', globalNav.changeMap);
			$('.institutions .map-link').bind('mouseout', globalNav.cleanMap);
			$('.hide_list-link').bind('click', globalNav.hideList);
			$('.map').bind('click', globalNav.showHelp);
			$('.instructions.error').bind('click', globalNav.hideHelp);

		},
		
		openMoreMenu : function() {

			$(this).parent().toggleClass('active');
			$('.iasd-global_navbar-more').toggleClass('open');
			$('.iasd-search-box').removeClass('open');
			return false;

		},
		
		closeMoreMenu : function() {

			$('.iasd-global_navbar-more').removeClass('open');
			return false;

		},

		openResponsiveMenu : function() {

			$('body, html').toggleClass('prevent-scrolling');
			$('.iasd-global_navbar-main .nav-collapse').toggleClass('open');
			$(this).closest('.iasd-global_navbar-main').toggleClass('open');
			if( $('.iasd-global_navbar-more').hasClass('open')){
				$('.iasd-global_navbar-more').removeClass('open');
				$('.iasd-global_navbar-more .tab-content').removeClass('open');
			}
			$('.iasd-search-box').removeClass('open');

		},

		openTabsMenu : function(){

			$('.iasd-global_navbar-more .tab-content').addClass('open');

		},

		closeTabsMenu : function(){

			$('.iasd-global_navbar-more .tab-content').removeClass('open');
			return false;

		},

		hideList : function(){
			$(this).closest('ul').removeClass('open');
		},

		openSearchForm : function(){

			$('.open-more-link').parent().removeClass('active');
			$('.iasd-search-box').toggleClass('open');
			$('.iasd-global_navbar-more').removeClass('open');
			$('body, html').removeClass('prevent-scrolling');
			$('.iasd-global_navbar-main .nav-collapse').removeClass('open');
			$('.btn-navbar').closest('.iasd-global_navbar-main').removeClass('open');			
			return false;

		},

		openInstitucionsList : function(){

			var jqThis = $(this);
			var region = jqThis.attr('data-region');
			$('.institutions ul ul').removeClass('open');
			$('.institutions .open-associations-link').removeClass('active');
			jqThis.addClass('active');
			$('.map').addClass('global-nav-map-'+region);
			jqThis.siblings('ul').addClass('open');
			$('.first-steps').remove();
			$('.instructions.error').remove();
			return false;

		},

		changeMap : function(){
			var jqThis = $(this).attr('data-region');
			$('.map').attr('class', 'map visible-desktop');
			$('.map').addClass('global-nav-map-'+jqThis);
		},

		cleanMap : function(){
			var jqThis = $('.institutions .open-associations-link');
			var region = $('.institutions .open-associations-link.active').attr('data-region');
			if(jqThis.hasClass('active')){
				$(".map").attr('class', 'map visible-desktop map global-nav-map-'+region);
			} else{
				$(".map").attr('class', 'map visible-desktop map global-nav-map-map-01');
			}
		},

		showHelp : function(){
			$('.instructions.error').css('visibility', 'visible');
		},

		hideHelp : function(){
			$('.instructions.error').remove();
		}		

	};

})(jQuery);