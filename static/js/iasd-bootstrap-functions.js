(function($) {
	$('.back-to-top a').click(function(eventObject) {
		eventObject.preventDefault();
		$('body,html').animate({ scrollTop: 0 }, 500);
		return false;
	});

	$('.header-top .nav.nav-hover .dropdown').hover(function (eventObject) {
		if($('.header-top').width() >= 979)
			$(this).addClass('open');
	}, function (eventObject) {
		if($('.header-top').width() >= 979)
			$(this).removeClass('open');
	});
	$('.nav-principal .btn.btn-navbar').click(function () {
		var btnNavbarAtual = $(this);
		if(btnNavbarAtual.hasClass('collapsed')) { //Vai abrir
			var btnNavbars = $('.nav-principal .btn.btn-navbar');
			for (var i = btnNavbars.length - 1; i >= 0; i--) {
				var btnNavbar = $(btnNavbars[i]);
				if(btnNavbar.attr('data-target') != btnNavbarAtual.attr('data-target')) {
					if(!btnNavbar.hasClass('collapsed')) {
						var dataTarget = $(btnNavbar.attr('data-target'));
						dataTarget.collapse('toggle');
						btnNavbar.addClass('collapsed');
					}
				}
			}
		}
		return true;
	});
  $('.sub-menu .dropdown a').click(function (eventObject) {
    var parent = $(this).parent();
    if(parent.hasClass('dropdown')) {
      parent.toggleClass('open');

      eventObject.preventDefault();
      eventObject.stopPropagation();
    } else {
      console.log(eventObject);
    }

    return true;
  });
})(jQuery);








