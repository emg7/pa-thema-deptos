<?php
/*
Template Name: Blog
*/
global $used_ids;
 get_header();
?>

<section id="departamentos-blog">
	<div class="container">
		<?php if(function_exists('breadcrumber')) breadcrumber(); ?>
		<div class="row">
			<div class="span8">

<?php
				$latest_taxonomy_main_highlight_query = new WP_Query(
					array (
						'posts_per_page' => 1,
						'post__not_in' => $used_ids,
						'meta_key' => 'all_highlight',
						'meta_value' => '2'
					)
				); //Posts from the taxonomy
				//var_dump($latest_taxonomy_main_highlight_query->request);

				if(isset($latest_taxonomy_main_highlight_query->posts[0])):
    						$used_ids[]=$latest_taxonomy_main_highlight_query->posts[0]->ID;
?>

				<div class="n-pic8-highlight">

					<a href="<?php echo get_permalink($latest_taxonomy_main_highlight_query->posts[0]->ID); ?>">
						<?php echo get_the_post_thumbnail($latest_taxonomy_main_highlight_query->posts[0]->ID, 'thumb_620x270'); ?>
						<div class="highlight-legend">
							<h4><?php echo $latest_taxonomy_main_highlight_query->posts[0]->post_title; ?></h4>
							<p><?php echo $latest_taxonomy_main_highlight_query->posts[0]->post_excerpt; ?></p>
						</div>
					</a>
				</div>
<?php 

				endif; 

				$latest_taxonomy_highlight_query = new WP_Query (
					array(
						'posts_per_page'=> 2,
						'post__not_in'=> $used_ids,
						'meta_key' => 'all_highlight',
						'meta_value' => '1'
					)
				);

				if( $latest_taxonomy_highlight_query -> have_posts() ):

?>
				<div class="row-fluid">
					<div class="span6 n-pic4-excerpt">
<?php
				if(isset($latest_taxonomy_highlight_query->posts[0])):
				    $used_ids[]=$latest_taxonomy_highlight_query->posts[0]->ID;
?>

                            <a href="<?php echo get_permalink($latest_taxonomy_highlight_query->posts[0]->ID); ?>"><?php echo get_the_post_thumbnail($latest_taxonomy_highlight_query->posts[0]->ID, 'thumb_300x170');?></a>
                            <a href="<?php echo get_permalink($latest_taxonomy_highlight_query->posts[0]->ID); ?>">
                                <?php echo $latest_taxonomy_highlight_query->posts[0]->post_title; ?></a>
                            <p><?php echo $latest_taxonomy_highlight_query->posts[0]->post_excerpt; ?></p>
                        <?php endif; ?>
					</div>
					<div class="span6 n-pic4-excerpt">
                        <?php if(isset($latest_taxonomy_highlight_query->posts[1])): ?>
						<?php $used_ids[]=$latest_taxonomy_highlight_query->posts[1]->ID; ?>
						<a href="<?php echo get_permalink($latest_taxonomy_highlight_query->posts[1]->ID); ?>"><?php echo get_the_post_thumbnail($latest_taxonomy_highlight_query->posts[1]->ID, 'thumb_300x170');?></a>
						<a href="<?php echo get_permalink($latest_taxonomy_highlight_query->posts[1]->ID); ?>">
							<?php echo $latest_taxonomy_highlight_query->posts[1]->post_title; ?></a>
						<p><?php echo $latest_taxonomy_highlight_query->posts[1]->post_excerpt; ?></p>
                        <?php endif; ?>
					</div>
				</div>
			<?php endif; ?>

				<div class="latest-news-cat">
					<h2><?php _e('Últimas postagens', 'thema_deptos'); ?></h2>
					<ul class="unstyled">

<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$latest_taxonomy_query = new WP_Query (array ('posts_per_page'=> 10, 'post__not_in'=> $used_ids, 'paged'=>$paged)); //Posts from the taxonomy

foreach ($latest_taxonomy_query->posts as $latest_taxonomy_post):

?>
						<li class="n-pic2-list row-fluid">
							<?php $used_ids[]=$latest_taxonomy_post->ID;  ?>
							<a href="<?php echo get_permalink($latest_taxonomy_post->ID); ?>"><?php echo get_the_post_thumbnail($latest_taxonomy_post->ID, 'thumb_140x90', array('class'=>'span3')); ?></a>
							<a href="<?php echo get_permalink($latest_taxonomy_post->ID); ?>"><?php echo $latest_taxonomy_post->post_title; ?></a>
							<p><?php echo $latest_taxonomy_post->post_excerpt; ?></p>
						</li>

<?php
endforeach;
?>

					</ul>
				</div>

<?php wp_pagenavi( array( 'query' => $latest_taxonomy_query ));  ?>

			</div>
			<div class="span4">
                <?php dynamic_sidebar('sidebar-blog-archive'); ?>
			</div>
		</div>
	</div> <!-- End Container -->
</section>

<?php get_footer(); ?>