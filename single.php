<?php
	get_header(); 

	if(have_posts())
		the_post();

	global $post;
	$post_type = get_post_type_object($post->post_type);
	$archive_link = get_post_type_archive_link( $post->post_type );

	$post_title = get_the_title();
	$post_link = get_permalink();
?>

<section class="departamentos-single">
	<div class="container">
		<?php if(function_exists('breadcrumber')) breadcrumber(); ?>
		
		<div class="row">
			<div class="span8 news-body">
				<small><?php the_date(); ?></small>
				<small><?php //the_author(); ?></small>
				<h1><?php single_post_title(); ?></h1>
				<?php  MasterThemeController::SocialWidgets($post_title, $post_link); ?>
				<p><?php the_content(); ?></p>
				<?php MasterThemeController::SocialWidgets($post_title, $post_link);  ?>
				<hr/>

				<div class="comments">
					<?php comments_template(); ?>
				</div>

			</div>
			<div class="span4">
			<?php dynamic_sidebar('sidebar-blog-single'); ?>
			</div>
			</div>
		</div>
	</div> <!-- End Container -->
</section>

<?php get_footer(); ?>