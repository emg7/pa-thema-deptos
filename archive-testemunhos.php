<?php
/*
Template Name: Testemunhos
*/

get_header(); ?>

<section id="archive-testemunhos">
	<div class="container">
		<?php if(function_exists('breadcrumber')) breadcrumber(); ?>
		<?php the_post(); ?>
		<h1><?php _e('Testemunhos', 'thema_deptos');?></h1>
		<div class="row testemunho-highlight">

<?php
	$ids = array();
	$testemunhos_highlight = new WP_Query (array (
		'type'=>'post',
		'post_type'=>'testemunhos',
		'posts_per_page'=> 1,
		'orderby'=> 'meta_value',
		'meta_key'=> 'testemunhos_highlight',
		'meta_value'=> '1',
		'meta_compare' => '>='
		)); //Posts from the taxonomy
	$pagenum = get_query_var('paged');
	if($pagenum == 0):
		while ( $testemunhos_highlight->have_posts() ) : $testemunhos_highlight->the_post();
				$ids[] = get_the_ID();
?>
		<div class="span6">
			<?php if(has_post_thumbnail()){ the_post_thumbnail(); ?>
				<span class="wp-caption-text"><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></span>
			<?php } ?>
		</div>

		<div class="span6 testemunho-highlight-info">
			<h3><?php _e('Destaque', 'thema_deptos');?></h3>

			<h2><?php echo the_title(); ?></h2>
			<p><?php echo the_excerpt_x(); ?></p>
			<a class="more-testemunhos" href="<?php the_permalink(); ?>"><?php _e('Ler mais', 'thema_deptos');?></a>
		</div>

		<hr class="span12" />


<?php
endwhile;
endif;
?>

		</div>


<?php
wp_reset_query();
$latest_testemunhos = new WP_Query (array (
	'type'=>'post',
	'post_type'=> 'testemunhos',
	'posts_per_page'=> 8,
	'orderby'=> 'date',
	'post__not_in'=> $ids,
	'paged' => get_query_var('paged')
	)); //

$i = 0;

while ( $latest_testemunhos->have_posts() ) : $latest_testemunhos->the_post();
	$ids[] = get_the_ID();
	if($i%2==0){
		echo ($i > 0) ? '<hr class="span12" /></div>' : "";
		echo '<div class="row testemunho-item">';
	}
?>

			<div class="span6">
				<div class="row">
					<div class="span2">
<?php
if(has_post_thumbnail()){
	the_post_thumbnail('testesmunhos_img');
}else{
?>
	<img src="<?php echo get_template_directory_uri(); ?>/static/img/testemunhos-generic-img.png" />
<?php
}
?>
					</div>
					<div class="span4">
						<h2><?php echo the_title(); ?></h2>
						<p><?php echo the_excerpt_x(240); ?></p>
						<div><a class="more-testemunhos" href="<?php the_permalink(); ?>"><?php _e('Ler mais', 'thema_deptos');?></a></div>
					</div>
				</div>
			</div>

<?php
	$i++;
	endwhile; wp_reset_query();
?>

		</div>

		<?php wp_pagenavi(array( 'query' => $latest_testemunhos )); ?>
	</div>
</section>

<?php get_footer(); ?>