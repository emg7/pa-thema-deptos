				<div class="header-top">
					<div class="logo-search-parent">
						<div class="container logo-search">
							<div class="row">
								<div class="span8 logo-container">
									<a href="<?php echo home_url(); ?>">
										<div class="logo"></div>
									</a>
									<h2><a href="<?php echo home_url(); ?>"><?php bloginfo(); ?></a></h2>
								</div>
								<div class="span4 search-container">
									<?php //get_search_form(); ?>
								</div>
							</div>
						</div>
					</div>
					<!--- Nav Principal -->
					<div class="nav-principal-parent">
						<div class="wrap container nav-principal">
							<div class="navbar">
								<div class="navbar-inner">
									<div class="container">
										<button type="button" class="btn btn-navbar btn-navbar-search " data-toggle="collapse" data-target=".menu-search-collapse">
											<div class="iasd-icons search"></div>
										</button>
										<div class="btn-spacer"></div>
										<button type="button" class="btn btn-navbar btn-navbar-idioma " data-toggle="collapse" data-target=".menu-idioma-collapse">
											<div class="iasd-icons i18n"><?php echo strtoupper(substr( WPLANG , 0, 2)); ?></div>
										</button>
										<div class="btn-spacer"></div>
										<button type="button" class="btn btn-navbar btn-navbar-principal " data-toggle="collapse" data-target=".menu-principal-collapse">
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
										</button>

										<?php
											$menuOpts = array(
													'menu'            => 'menu-principal',
													'container_class' => 'nav-collapse collapse menu-principal-collapse',
													'echo'            => true,
													'fallback_cb'     => 'wp_page_menu',
													'depth'           => 0,
													'items_wrap'      => '<ul id="%1$s" class="nav %2$s">%3$s</ul>'
											);

//											if (class_exists("MainNavMenuWalker"))
											$menuOpts['walker'] = new MainNavMenuWalker(TRUE);
											
											wp_nav_menu( $menuOpts );
										?>
										<div class="nav-collapse collapse menu-idioma-collapse">
											<ul class="i18n-nav nav pull-right">
												<li id="menu-item-i18n-1" class="menu-item i18n has_children menu-item-i18n-1 dropdown">
													<a href="javascript:;" class="dropdown-toggle iasd-icons i18n"><?php echo strtoupper(substr( WPLANG , 0, 2)); ?></a>
													<ul class="sub-menu dropdown-menu">
														<?php
															if(class_exists('LanguageController')):

																$languagesURLs = get_option('pa_i18n_urls');
																foreach (LanguageController::$languages as $key => $lang) {
																	if ( $lang != $currentLang && !empty($languagesURLs[$lang]) ){
																			echo '<li id="menu-item-i18n-'.$key.'" class="menu-item i18n menu-item-i18n-'.$key.'"><a href="'.$languagesURLs[$lang].'">'.strtoupper(substr( $lang , 0, 2)).'</a></li>';
																	}
																}
															endif;
														?>
													</ul>
												</li>
											</ul>
										</div>
										<div class="nav-collapse collapse menu-search-collapse">
											<div class="row">
												<div class="span4 fullspan">
													<?php get_search_form(); ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>