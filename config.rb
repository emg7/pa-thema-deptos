# Require any additional compass plugins here.
gem 'iasd-bootstrap-sass', '=0.0.3'
require "iasd-bootstrap-sass"

# Set this to the root of your project when deployed:
http_path       = ""
images_dir      = "static/img"
javascripts_dir = "static/js"
css_dir         = "."
sass_dir        = "sass"

#other sass used on the install
add_import_path "../departamentos_theme/sass"

relative_assets = true

output_style = (environment == :production) ? :compressed : :expanded