<?php
/*
Template Name: Projetos
*/
get_header(); ?>

<section id="archive-projetos">
	<div class="container">
		<?php if(function_exists('breadcrumber')) breadcrumber(); ?>
		<div class="projetos-page-content">

			<?php
			if (! is_post_type_archive( 'projetos' )){
				the_post();
				the_title('<h1>', '</h1>');
				the_content();
			}
			?>


		</div>
		<div class="row projetos-row">
<?php
			$i = 1;
			$latest_projetos = new WP_Query (array ('type'=>'post', 'posts_per_page' => '-1' , 'post_type'=> 'projetos', 'orderby'=> 'date')); //
			if($latest_projetos->have_posts()) : while ( $latest_projetos->have_posts() ) : $latest_projetos->the_post();

?>
			<div class="span6">
				<?php if (has_post_thumbnail( )) : ?>
					<a class="projeto-item thumbnail-link" href="<?php the_permalink(); ?>"><?php echo the_post_thumbnail( 'thumb_460x200' ); ?></a>
				<?php else: ?>
					<a class="projeto-item thumbnail-link" href="<?php the_permalink(); ?>"><img src="http://dummyimage.com/460x200/cccccc/666666.png&text=Sem+imagem+de+destaque" /></a>
				<?php endif; ?>

				<a class="projeto-item" href="<?php the_permalink(); ?>"><h3><?php echo the_title(); ?></h3></a>
				<?php echo (function_exists('get_the_excerpt_x')) ? get_the_excerpt_x() : get_the_excerpt(); ?>
			</div>
<?php
		if($i % 2 == 0) { echo '</div><div class="row projetos-row">';} 
			$i++; endwhile; endif; wp_reset_query();
?>
		</div>
	</div>
</section>

<?php get_footer(); ?>