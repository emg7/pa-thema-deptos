<?php 

/*
Template Name: Lideranças
*/

$lideres = 'SELECT 
	    wp_terms.*, wp_posts.*
	FROM wp_posts 
	    LEFT JOIN wp_term_relationships ON wp_posts.ID = wp_term_relationships.object_id
	    LEFT JOIN wp_term_taxonomy ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
	    LEFT JOIN wp_terms ON wp_term_taxonomy.term_id = wp_terms.term_id
		WHERE 
		    wp_terms.slug IS NOT NULL
		    AND 
		    wp_posts.post_type = "lideres"';
$lideres_results = $wpdb->get_results( $wpdb->prepare(  $lideres ) );
$lideres_items = array();

foreach ($lideres_results as $v)
	$lideres_items[$v->slug] = $v;

//var_dump($lideres_items);

function mostraDadosLider($conference = '') {
	if(!$conference)
		return false;

	global $lideres_items;

	if(!isset($lideres_items[$conference]))
		return false;

	$lider = $lideres_items[$conference];
	?>

			<div class="span4">
				<a class="lideres-img" href="#"><?php echo get_the_post_thumbnail($lider->ID, 'lideres_image'); ?></a>
				<div class="lideres-content">
					<a href="#"><strong><?php echo $lider->post_title; ?></strong></a>
					<a class="uniao" href="#"><?php echo $lider->name; ?></a>
					<ul class="unstyled">
						<?php 
						$social_network_email = get_post_meta($lider->ID, 'social-networks_email', true);
						if ( $social_network_email ):
						 ?>
						<li>
							<a class="email-lideres" href="mailto:<?php echo $social_network_email; ?>" rel="tooltip" data-placement="top" data-original-title="<?php echo $social_network_email; ?>"></a>
						</li>
						<?php 
						endif;
						$social_network_twitter = get_post_meta($lider->ID, 'social-networks_twitter', true);
						if ( $social_network_twitter ):
						 ?>
						<li>
							<a class="twitter-lideres" href="http://www.twitter.com/<?php echo $social_network_twitter; ?>" rel="tooltip" data-placement="top" data-original-title="$social_network_twitter; ?>"></a>
						</li>
						<?php 
						endif;
						$social_network_facebook = get_post_meta($lider->ID, 'social-networks_facebook', true);
						if ( $social_network_facebook ):
						 ?>
						<li>
							<a class="facebook-lideres" href="http://www.facebook.com/<?php echo $social_network_facebook; ?>" rel="tooltip" data-placement="top" data-original-title="<?php echo $social_network_facebook; ?>"></a>
						</li>
					<?php endif; ?>
					</ul>
					<a class="saber-mais" href="#">Saiba mais</a>
				</div>
			</div>
	<?php
	return true;
}

get_header(); ?> 

<section class="departamentos-liderancas">
	<div class="container">
		<h2>Lideranças</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce malesuada fermentum elit, at dapibus urna vehicula sit amet. Aliquam sed purus vel nisl feugiat blandit. Donec eget odio at felis ultrices euismod. Duis tellus lacus, mattis ac eleifend et, vehicula at felis. Phasellus mattis sapien sit amet enim condimentum vestibulum.</p>


		<?php

		$lider = $lideres_items['divisao-sul-americana'];

		 ?>


		<div id="lider-sul-americana" class="row-fluid">
			<h3>Líder da divisão sul-americana</h3>
			<a class="span4 lideres-img" href="#"><?php echo get_the_post_thumbnail($lider->ID, array(300,270)); ?></a>
			<div class="span4 lideres-content">
				<a href="#"><strong><?php echo $lider->post_title; ?></strong></a>
				<?php echo $lider->post_content; ?>
				<ul class="unstyled">
					<li><a href="mailto:<?php echo get_post_meta($lider->ID, 'social-networks_email', true); ?>"><span class="email-lideres"></span><?php echo get_post_meta($lider->ID, 'social-networks_email', true); ?></a></li>
					<li><a href="http://www.twitter.com/<?php echo get_post_meta($lider->ID, 'social-networks_twitter', true); ?>"><span class="twitter-lideres"></span><?php echo get_post_meta($lider->ID, 'social-networks_twitter', true); ?></a></li>
					<li><a href="http://www.facebook.com/<?php echo get_post_meta($lider->ID, 'social-networks_facebook', true); ?>"><span class="facebook-lider-sul-americana"></span><?php echo get_post_meta($lider->ID, 'social-networks_facebook', true); ?></a></li>
				</ul>
				<a class="saber-mais" href="#">Saiba mais</a>
			</div>
		</div>

		<div class="row lideres">
			<h3>Líderes das Uniões</h3>
				<?php 
					mostraDadosLider('uniao-argentina');
					mostraDadosLider('uniao-boliviana');
					mostraDadosLider('uniao-central-brasileira');
				?>
		</div>
		<div class="row lideres">
				<?php 
					mostraDadosLider('uniao-centro-oeste-brasileira');
					mostraDadosLider('uniao-chilena');
					mostraDadosLider('uniao-equatoriana');
				?>
		</div>
		<div class="row lideres">
				<?php 
					mostraDadosLider('uniao-nordeste-brasileira');
					mostraDadosLider('uniao-noroeste-brasileira');
					mostraDadosLider('uniao-norte-brasileira');
				?>
		</div>
		<div class="row lideres">
				<?php 
					mostraDadosLider('uniao-paraguaia');
					mostraDadosLider('uniao-peruana-do-norte');
					mostraDadosLider('uniao-peruana-do-sul');
				?>
		</div>
		<div class="row lideres">
				<?php 
					mostraDadosLider('uniao-sudeste-brasileira');
					mostraDadosLider('uniao-sul-brasileira');
					mostraDadosLider('uniao-uruguaia');
				?>
		</div>
			<!--div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Pr. Carlos Campitelli</strong></a>
					<a class="uniao" href="#">União Argentina</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Pr. Jimmy Lafuente</strong></a>
					<a class="uniao" href="#">União Boliviana</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Pr. Ronaldo Arco</strong></a>
					<a class="uniao" href="#">União Central Brasileira</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
		</div>
		<div class="row lideres">
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Nelson Milanelli</strong></a>
					<a class="uniao" href="#">União Centro-Oeste Brasileira</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>David Victoriano</strong></a>
					<a class="uniao" href="#">União Chilena</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Pr. Juan Cancino</strong></a>
					<a class="uniao" href="#">União Equatoriana</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
		</div>
		<div class="row lideres">
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Sosthenes Pereira Andrade</strong></a>
					<a class="uniao" href="#">União Nordeste Brasileira</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Donato Azevedo Filho</strong></a>
					<a class="uniao" href="#">União Noroeste Brasileira</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Herbert Almeida</strong></a>
					<a class="uniao" href="#">União Norte Brasileira</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
		</div>
		<div class="row lideres">
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Benjamin Belmonte</strong></a>
					<a class="uniao" href="#">União Paraguaia</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Pr. Jaime Perez</strong></a>
					<a class="uniao" href="#">União Peruana do Norte</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Pr. Miduart Montesinos</strong></a>
					<a class="uniao" href="#">União Peruana do Sul</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
		</div>
		<div class="row lideres">
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Pr. Ivay Araújo</strong></a>
					<a class="uniao" href="#">União Sudeste Brasileira</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Elmar Borges</strong></a>
					<a class="uniao" href="#">União Sul Brasileira</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
			<div class="span4">
				<a class="lideres-img" href="#"><img src="http://www.placehold.it/140x140" /></a>
				<div class="lideres-content">
					<a href="#"><strong>Pr. Lorem Ipsum</strong></a>
					<a class="uniao" href="#">União Uruguaia</a>
					<ul class="unstyled">
						<li><a class="email-lideres" href="#"></a></li>
						<li><a class="twitter-lideres" href="#"></a></li>
						<li><a class="facebook-lideres" href="#"></a></li>
					</ul>
					<a class="saber-mais" href="#">Saber mais</a>
				</div>
			</div>
		</div-->
	</div>
</section>

<?php get_footer(); ?>