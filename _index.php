<?php get_header(); ?>

<section id="departamentos-index">
	<div class="container">
		<div class="row">
			<div class="span8">
				<?php dynamic_sidebar('home-row-1-left'); ?>
			</div>
			<div class="span4">
				<?php dynamic_sidebar('home-row-1-right'); ?>
			</div>
		</div>
		<div class="row">
			<div class="span8">
				<?php dynamic_sidebar('home-row-2-left'); ?>
			</div>
			<div class="span4 ministerio-jovem-videos">
				<h2>Vídeos</h2>
				<div class="ministerio-jovem-videos-inner">
					<img src="http://www.placehold.it/300x170" />
					<a href="#">Somos Teus - CD Jovem 2012</a>
					<ul class="unstyled">
						<li>
							<img src="http://www.placehold.it/60x35" />
							<a href="#">Testemunho linod do Ministério Jovem da Iasd Guará - UCOB</a>
						</li>
						<li>
							<img src="http://www.placehold.it/60x35" />
							<a href="#">LIFE Identidade - 17 de Setembro - Dia do Jovem Adventista</a>
						</li>
					</ul>
				</div>
				<a class="more-button pull-right" href="#">Veja mais vídeos</a>
			</div>
		</div>
		<div class="row">
			<div class="span4">
				<h2>Eventos</h2>
				<div class="departamentos-eventos">
				  	<ul class="unstyled">
				  		<li>
				  			<span><b>22/09</b><p>14h-16h</p></span>
				  			<a href="#"><b>Treinamento de pastores</b><br/>IASD São Paulo central</a>
				  		</li>
				  		<li>
				  			<span><b>22/09</b><p>14h-16h</p></span>
				  			<a href="#"><b>Reunião ministério jovem</b><br/>IASD Av. Paulista</a>
				  		</li>
				  		<li>
				  			<span><b>22/09</b><p>14h-16h</p></span>
				  			<a href="#"><b>Festa junina desbravadores</b><br/>Rua 7 de setembro, 854</a>
				  		</li>
				  		<li>
				  			<span><b>22/09</b><p>14h-16h</p></span>
				  			<a href="#"><b>Treinamento de pastores</b><br/>IASD Brasília central</a>
				  		</li>
				  		<li>
				  			<span><b>22/09</b><p>14h-16h</p></span>
				  			<a href="#"><b>Envontro de jovens adventistas</b><br/>IASD São Paulo central</a>
				  		</li>
				  		<li>
				  			<span><b>22/09</b><p>14h-16h</p></span>
				  			<a href="#"><b>Envontro de jovens adventistas</b><br/>IASD São Paulo central</a>
				  		</li>
				  	</ul>
				  	<a class="see-more" href="#">Veja mais eventos</a>
				</div>
			</div>
			<div class="span4">
				<?php dynamic_sidebar('home-row-3-middle'); ?>
			</div>
			<div class="span4">
				<?php dynamic_sidebar('home-row-3-right'); ?>
			</div>
		</div>
		<div class="row">
			<div class="span4 pic1-gallery">
				<h2>Galeria de Imagens</h2>
				<ul class="unstyled">
					<li class="pic1-gallery-inner">
						<a href="#">
							<div class="pic1-gallery-img">
								<img src="http://www.placehold.it/74x49" />
							</div>
							<div>
								<b>Vidas por Vidas</b>
								<p>Central de Salvador</p>
								<time>25/09/2012</time>
							</div>
						</a>
					</li>
					<li class="pic1-gallery-inner">
						<a href="#">
							<div class="pic1-gallery-img">
								<img src="http://www.placehold.it/74x49" />
							</div>
							<div>
								<b>Dia do Jovem Adventista</b>
								<p>Associação Norte Paranaense</p>
								<time>15/09/2012</time>
							</div>
						</a>
					</li>
					<li class="pic1-gallery-inner">
						<a href="#">
							<div class="pic1-gallery-img">
								<img src="http://www.placehold.it/74x49" />
							</div>
							<div>
								<b>IV Concenção do Ministério Jovem ACSR</b>
								<p>Central de Novo Hamburgo</p>
								<time>16/03/2012</time>
							</div>
						</a>
					</li>
					<li class="pic1-gallery-inner">
						<a href="#">
							<div class="pic1-gallery-img">
								<img src="http://www.placehold.it/74x49" />
							</div>
							<div>
								<b>Impacto Esperança é realizado em Santo Cristo</b>
								<p>Ministério Jovem de Santa Rosa</p>
								<time>15/03/2012</time>
							</div>
						</a>
					</li>
					<li class="pic1-gallery-inner">
						<a href="#">
							<div class="pic1-gallery-img">
								<img src="http://www.placehold.it/74x49" />
							</div>
							<div>
								<b>Encontro de filhos Obreiros</b>
								<p>Instituto Adventista Paranaense</p>
								<time>23/02/2012</time>
							</div>
						</a>
					</li>
				</ul>
				<a class="more-button" href="#">Veja mais imagens</a>
			</div>
			<div class="span4 ">
			</div>
			<div class="span4">
				<h2>RSS Projetos</h2>
				<div class="departamentos-rss-projetos">
				  	<ul class="unstyled">
				  		<li>
				  			<span></span>
				  			<a href="#"><b>Vidas lalala por Vidas</b><br/>www.vidasporvidas.com</a>
				  		</li>
				  		<li>
				  			<span></span>
				  			<a href="#"><b>Conexão Jovem</b><br/>novotempo.com/conexaojovem</a>
				  		</li>
				  		<li>
				  			<span></span>
				  			<a href="#"><b>Estilo JA</b><br/>www.estiloja.com</a>
				  		</li>
				  		<li>
				  			<span></span>
				  			<a href="#"><b>Missão Jovem</b><br/>www.missaojovem.org</a>
				  		</li>
				  		<li>
				  			<span></span>
				  			<a href="#"><b>Centro de Evangelismo Jovem</b><br/>www.evangelismojovem.com.br</a>
				  		</li>
				  		<li>
				  			<span></span>
				  			<a href="#"><b>Centro de Evangelismo Jovem</b><br/>www.evangelismojovem.com.br</a>
				  		</li>
				  	</ul>
				  	<a class="see-more" href="#">Veja mais eventos</a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>