<?php

require_once 'LideresController.class.php';
require_once 'ContatosController.class.php';
require_once 'ProjetosController.class.php';
require_once 'TestemunhosController.class.php';

require_once 'widgets/Noticias4RecentesWidget.class.php';
require_once 'widgets/SliderDestaquesNivel3Widget.class.php';
require_once 'widgets/Botoes3DestaquesWidget.class.php';
require_once 'widgets/ProjetoSocialLinksWidget.class.php';
require_once 'widgets/ProjetoContatosWidget.class.php';
require_once 'widgets/ProjetoFaqWidget.class.php';
require_once 'widgets/TestemunhosLatestWidget.class.php';
require_once 'widgets/NossosProjetosWidget.class.php';
require_once 'widgets/NossosArtigosWidget.class.php';
require_once 'widgets/CustomBotoes3DestaquesWidget.class.php';


class MasterThemeController {
	static $HighlightLevels = array('Normal', 'Nível 1', 'Nível 2', 'Nível 3');

	public static function ManagePostTypeHighlightColumns($columns) {
		$columns = array_merge($columns, array(
			'post_type_highlight' => __('Destaque', 'thema_deptos')
		));
		return $columns;
	}

	public static function ManagePostTypeHighlightCustomColumn($column, $postId) {
		switch ($column) {
			case 'post_type_highlight': {
				global $post;
				$post_type = $post->post_type;

				if(has_post_thumbnail($postId)){
					$highlight_level = strtolower(get_post_meta($postId, $post_type.'_highlight', true));

					echo '<select class="post_type_highlight" data-posttype="'.$post_type.'" data-postid="'.$postId.'">';

					foreach(self::$HighlightLevels as $id => $level):
						echo '<option value="'.$id.'" '.($highlight_level == $id ? 'selected="selected"' : ''  ).'>'.$level.'</option>';
					endforeach;

					echo '</select>';
				}else{
					echo __('Post sem imagem.', 'thema_deptos');
				}
				break;
			}
		}
	}

	public static function TogglePostTypeHighlight(){
		$post_type = $_POST['post_type'];
		update_post_meta($_POST['post_id'], $post_type.'_highlight', $_POST['new_value']);
		update_post_meta($_POST['post_id'], 'all_highlight', $_POST['new_value']);
		die;
	}

	public static function MasterInit() {
		add_theme_support( 'post-thumbnails' );

		self::MasterThumbSize();

		self::MasterRegisterSidebar();

	}

	public static function i18nSetup(){
		// load_theme_textdomain('thema_deptos', get_template_directory() . '/languages/');
	}

	public static function SetSecaoTaxonomy( $post_ID ) {
		wp_set_object_terms( $post_ID, 'Institucional', 'xtt-pa-secao' );
	}

	public static function returnSite($classes) {
		$site = 'institucional';
		array_push($classes, $site);
		return $classes;
	}

	public static function custom_menu_order($menu_ord) {
		if (!$menu_ord) return true;
		
		return array(
			'index.php',
			'separator1',
			'edit.php',
			'edit.php?post_type=projetos',
			'edit.php?post_type=contatos',
			'edit.php?post_type=qa_faqs',
			'edit.php?post_type=pa_image_gallery',
			'edit.php?post_type=lideres',
			'edit.php?post_type=colunas',
			'edit.php?post_type=testemunhos',
			'edit.php?post_type=page',
			'link-manager.php',
	/*
			'separator1',

			'upload.php',

			'edit-comments.php',
			'separator2',
			'themes.php',
			'plugins.php',
			'users.php',
			'tools.php',
			'options-general.php',
			'separator-last',
	*/
		);
	}

	public static function PostTypeLink( $post_link, $post = null, $leavename = null, $sample = null )
	{
		if(is_null($post)) {
			$object = get_queried_object();
			if(get_class($object) == 'WP_Post')
				$post = $object;
		}
		if($post) {
			if ( $post->post_type == PAImageGallery::$post_type_name ) {
				$terms = wp_get_object_terms($post->ID, PATaxonomias::TAXONOMY_PROJETOS);
				if(!$terms)
					$terms = array();
				$term = reset($terms);
				if($term) {
					$args = array('tax_query' => array(), 'post_type' => 'projetos');
					$args['tax_query'][] = array('taxonomy' => PATaxonomias::TAXONOMY_PROJETOS, 'field' => 'id', 'terms' => $term->term_id);
					$projetos = new WP_Query($args);
					if(count($projetos->posts)) {
						$projeto = reset($projetos->posts);
						$post_link = str_replace( '%projeto%', $projeto->post_name, $post_link );
					}
				}
			} else if( $post->post_type == 'projetos' ) {
				$post_link = str_replace( '%projeto%', $post->post_name, $post_link );
			}
		}
		if(strpos($post_link, ProjetosController::SingleSlug() . '/%projeto%/'))
			$post_link = str_replace( ProjetosController::SingleSlug() . '/%projeto%/', '', $post_link );


		return $post_link;
	}
	public static function ImageGallerySlugArchive( $current )
	{
		$slug = ProjetosController::SingleSlug() . '/%projeto%/galerias';
		$rewrite_regex = str_replace('%projeto%', '([^/]+)', $slug.'/?$');
		$rewrite_redirect = 'index.php?post_type='.PAImageGallery::$post_type_name.'&projeto=$matches[1]';
		add_rewrite_rule( $rewrite_regex, $rewrite_redirect, 'top');
		flush_rewrite_rules();

		return $slug;
	}
	public static function ImageGallerySlugSingle( $current )
	{
		$slug = ProjetosController::SingleSlug() . '/%projeto%/galeria';

		//Rewrite correto
		$rewrite_regex = str_replace('%projeto%', '([^/]+)', $slug.'/([^/]+)/?$');
		$rewrite_redirect = 'index.php?name=$matches[2]&post_type='.PAImageGallery::$post_type_name.'&projeto=$matches[1]';
		add_rewrite_rule( $rewrite_regex, $rewrite_redirect, 'top');
		flush_rewrite_rules();

		return $slug;
	}
	public static function ImageGalleryFilter($args) {
		global $wp, $wp_query;
		$page_args = wp_parse_args($wp->matched_query);

		if(isset($page_args['projeto'])) {
			$projeto = get_page_by_path($page_args['projeto'], OBJECT, 'projetos');

			$terms = wp_get_object_terms($projeto->ID, PATaxonomias::TAXONOMY_PROJETOS);
			if(!$terms)
				$terms = array();
			$term = reset($terms);
			if($term) {
				if(!isset($args['tax_query']))
					$args['tax_query'] = array();
				$args['tax_query'][] = array('taxonomy' => PATaxonomias::TAXONOMY_PROJETOS, 'field' => 'id', 'terms' => $term->term_id);
			}
			$wp_query = new WP_Query($args);
		}

		return $args;
	}

	public static function MasterThumbSize() {
		if ( function_exists( 'add_image_size' ) ) {
			add_image_size( 'thumb_140x90', 140, 90, true ); //(cropped)
			add_image_size( 'thumb_60x60', 60, 60, true );
			add_image_size( 'thumb_460x140', 460, 140, true );
			add_image_size( 'thumb_460x200', 460, 200, true ); // Archive Projetos proporcional ao Slider da Home
			add_image_size( 'thumb_620x270', 620, 270, true );
			add_image_size( 'thumb_300x170', 300, 170, true ); // Single Projetos cropped
			add_image_size( 'thumb_300x300', 300, 300, true );
			add_image_size( 'thumb_300x200', 300, 200, true ); // Destaques do archive
			add_image_size( 'thumb_140x140', 140, 140, true );
		}
	}

	public static function MasterEnqueues() {

//		Já é adicionado automaticamente
//		wp_enqueue_style( 'wordpress_style', get_stylesheet_directory_uri() .'/style.css', false, false, 'all' );

		//Javascripts
		wp_enqueue_script( 'jquery', false, false, false, true );

	    wp_register_script( 'bootstrap', get_stylesheet_directory_uri().'/static/js/iasd-bootstrap.min.js', array('jquery'), '1.0.0', true);
	    wp_enqueue_script( 'bootstrap' );
	    wp_register_script( 'bootstrap-functions', get_stylesheet_directory_uri().'/static/js/iasd-bootstrap-functions.js', array('bootstrap'), '1.0.0', true);
	    wp_enqueue_script( 'bootstrap-functions' );
	    wp_register_script( 'dropdown-nav', get_stylesheet_directory_uri().'/static/js/iasd-dropdown-nav.js', array('jquery'), '1.0.0', true);
	    wp_enqueue_script( 'dropdown-nav' );
	    
	    wp_register_script( 'global-nav', get_stylesheet_directory_uri().'/static/js/iasd-global-nav.js', array('jquery'), '1.0.0', true);
	    wp_enqueue_script( 'global-nav' );

		wp_register_script( 'flexslider', get_template_directory_uri() .'/static/lib/flexslider/jquery.flexslider-min.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script( 'flexslider' );

		wp_register_script( 'departamentos', get_template_directory_uri() .'/static/js/departamentos.js', array('jquery' ), '1.0.0', true);
		wp_enqueue_script( 'departamentos' );
	}

	public static function MasterPrepareAdminPostView() {
		//get_template_directory_uri()
		wp_register_script( 'post_type_highlight', get_template_directory_uri().'/static/js/toggle_post_type_highlight.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script( 'post_type_highlight' );

	}

	public static function MasterRegisterSidebar(){

		if ( function_exists('register_sidebar') ) {

			register_sidebar(array(
				'name' => __('Home Linha 1: Esquerda', 'thema_deptos'),
				'id' => 'home-row-1-left',
				'description' => __('Espaço da home, à esquerda, na primeira linha', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
				));

			register_sidebar(array(
				'name' => __('Home Linha 1: Direita', 'thema_deptos'),
				'id' => 'home-row-1-right',
				'description' => __('Espaço da home, à direita, na primeira linha', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
				));

			register_sidebar(array(
				'name' => __('Home Linha 2: Esquerda', 'thema_deptos'),
				'id' => 'home-row-2-left',
				'description' => __('Espaço da home, à esquerda, na segunda linha', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
				));

			register_sidebar(array(
				'name' => __('Home Linha 2: Direita', 'thema_deptos'),
				'id' => 'home-row-2-right',
				'description' => __('Espaço da home, à direita, na segunda linha', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
				));

			register_sidebar(array(
				'name' => __('Home Linha 3: Esquerda', 'thema_deptos'),
				'id' => 'home-row-3-left',
				'description' => __('Espaço da home, à esquerda, na terceira linha', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
			));

			register_sidebar(array(
				'name' => __('Home Linha 3: Meio', 'thema_deptos'),
				'id' => 'home-row-3-middle',
				'description' => __('Espaço da home, no meio, na terceira linha', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
			));

			register_sidebar(array(
				'name' => __('Home Linha 3: Direita', 'thema_deptos'),
				'id' => 'home-row-3-right',
				'description' => __('Espaço da home, na direita, na terceira linha', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
			));

			register_sidebar(array(
				'name' => __('Home Linha 4: Esquerda', 'thema_deptos'),
				'id' => 'home-row-4-left',
				'description' => __('Espaço da home, à esquerda, na quarta linha', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
			));

			register_sidebar(array(
				'name' => __('Home Linha 4: Meio', 'thema_deptos'),
				'id' => 'home-row-4-middle',
				'description' => __('Espaço da home, no meio, na quarta linha', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
			));

			register_sidebar(array(
				'name' => __('Home Linha 4: Direita', 'thema_deptos'),
				'id' => 'home-row-4-right',
				'description' => __('Espaço da home, na direita, na quarta linha', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
			));

			register_sidebar(array(
				'name' => __('Sidebar: Planejamento', 'thema_deptos'),
				'id' => 'sidebar-planejamento',
				'description' => __('Appears as the sidebar on the custom homepage', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
				));
			register_sidebar(array(
				'name' => __('Sidebar: Testemunhos', 'thema_deptos'),
				'id' => 'sidebar-testemunhos',
				'description' => __('Appears as the sidebar on the custom homepage', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
				));
			register_sidebar(array(
				'name' => __('Sidebar: Páginas', 'thema_deptos'),
				'id' => 'sidebar-pagina',
				'description' => __('Sidebar para Paginas', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
				));
			/*
			register_sidebar(array(
				'name' => __('Blog: Sidebar', 'thema_deptos'),
				'id' => 'sidebar-blog',
				'description' => __('Appears as the sidebar on the custom homepage', 'thema_deptos'),
				'before_widget' => '',
				'after_widget' => '',
				'before_title' => '',
				'after_title' => '',
				));
			register_sidebar(array(
				'name' => __('Blog: Sidebar Post', 'thema_deptos'),
				'id' => 'sidebar-post-blog',
				'description' => __('Appears as the sidebar on the custom homepage', 'thema_deptos'),
				'before_widget' => '',
				'after_widget' => '',
				'before_title' => '',
				'after_title' => '',
				));
			register_sidebar(array(
				'name' => __('Blog: Arquivo', 'thema_deptos'),
				'id' => 'blog-archive',
				'description' => __('Widgets para a tela de artigos do blog', 'thema_deptos'),
				'before_widget' => '',
				'after_widget' => '',
				'before_title' => '',
				'after_title' => '',
				));
			*/

			register_sidebar(array(
				'name' => __( 'Sidebar: Blog Archive' ),
				'id' => 'sidebar-blog-archive',
				'description' => __('Widgets para o "ARCHIVE" do blog.', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
			));
			register_sidebar(array(
				'name' => __( 'Sidebar: Blog Single' ),
				'id' => 'sidebar-blog-single',
				'description' => __('Widgets para o "SINGLE" do blog.', 'thema_deptos'),
				'before_widget' => '<div class="widget">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
			));

		}
	}

	public static function SocialWidgets($title, $url){ ?>
		<div class="social-widgets row-fluid">
			<div class="span2"><div class="fb-like" data-send="false" data-layout="button_count" data-width="250" data-show-faces="false"></div></div>
			<div class="span2"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="en">Tweet</a></div>
			<div class="span2"><div class="g-plusone" data-size="medium" data-annotation="bubble" data-width="120"></div></div>
			<div class="span2 offset2 text-right"><a class="btn btn--share-email btn-mini" href="mailto:?subject=<?php echo $title; ?>&amp;body=<?php echo $url; ?>"><i class="icon-envelope"></i><?php _e('E-mail', 'thema_deptos'); ?></a></div>
			<div class="span2 text-right"><a class="btn btn--print btn-mini" href="javascript:window.print();"><i class="icon-print"></i><?php _e('Imprimir', 'thema_deptos'); ?></a></div>
		</div>
		<div id="fb-root"></div>
		<?php
		wp_enqueue_script('social-widgets', get_template_directory_uri() .'/static/js/social-widgets.js',array(),false,true);
	}

	public static function theTitleMaxCharlength($title, $charlength) {

		$charlength++;

		if ( mb_strlen( $title ) > $charlength ) {
			$subex = mb_substr( $title, 0, $charlength -5);
			$exwords = explode( ' ', $subex );
			$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo mb_substr( $subex, 0, $excut );
			} else {
				echo $subex;
			}
			echo '...';
		} else {
			echo $title;
		}
	}

	public static function LateInit() {
		global $wp_post_types;
		$wp_post_types['post']->label = __('Blog', 'thema_deptos');
	}

	public static function RegisterMenus() {
		register_nav_menus(
			array(
				'menu-principal' => __( 'Menu Principal', 'thema_deptos' ),
			)
		);
	}

	public static function RemoveCssJsVer( $src ) {
		if( strpos( $src, '?ver=' ) )
			$src = remove_query_arg( 'ver', $src );
		return $src;
	}

}

load_theme_textdomain('thema_deptos', get_template_directory() . '/languages/');

add_filter('custom_menu_order', array('MasterThemeController', 'custom_menu_order'));
add_filter('menu_order', array('MasterThemeController', 'custom_menu_order'));

add_action('wp_ajax_toggle_post_type_highlight', array('MasterThemeController', 'TogglePostTypeHighlight'));
add_action('admin_print_scripts', array('MasterThemeController', 'MasterPrepareAdminPostView'));

add_action('after_setup_theme',  array('MasterThemeController', 'i18nSetup'));
add_action( 'save_post', array('MasterThemeController', 'SetSecaoTaxonomy'));
add_filter( 'body_class', array('MasterThemeController', 'returnSite'));

add_action('manage_post_posts_custom_column', array('MasterThemeController', 'ManagePostTypeHighlightCustomColumn'), 10, 2);
add_filter('manage_post_posts_columns', array('MasterThemeController', 'ManagePostTypeHighlightColumns'));
add_action('manage_projetos_posts_custom_column', array('MasterThemeController', 'ManagePostTypeHighlightCustomColumn'), 10, 2);
add_filter('manage_projetos_posts_columns', array('MasterThemeController', 'ManagePostTypeHighlightColumns'));
add_filter('post_type_link', array('MasterThemeController', 'PostTypeLink'), 100, 4 );
add_filter('post_type_archive_link', array('MasterThemeController', 'PostTypeLink'), 100);


add_action( 'init', array('MasterThemeController', 'MasterInit') );
add_action( 'init', array('MasterThemeController', 'RegisterMenus') );
add_action( 'init', array('MasterThemeController', 'LateInit'), 100);
add_action( 'wp_enqueue_scripts',  array('MasterThemeController','MasterEnqueues'));

add_filter('image_gallery_filter_archive_optional', array('MasterThemeController', 'ImageGalleryFilter'));
add_filter('image_gallery_slug_single',  array('MasterThemeController', 'ImageGallerySlugSingle'));
add_filter('image_gallery_slug_archive', array('MasterThemeController', 'ImageGallerySlugArchive'));

add_filter( 'style_loader_src', array('MasterThemeController', 'RemoveCssJsVer'), 10, 2 );
add_filter( 'script_loader_src', array('MasterThemeController', 'RemoveCssJsVer'), 10, 2 );




