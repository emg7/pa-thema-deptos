<?php
class Noticias4RecentesWidget extends WP_Widget
{

	static function Init()
	{
		register_widget(__CLASS__);
	}

	function __construct()
	{
		$widget_ops = array('classname' => __CLASS__, 'description' => __('Lista os ultimos 4 posts locais (não ASN) do departamento', 'thema_deptos'));
		$this->WP_Widget(__CLASS__, __('Departamentos: Ultimos 4 posts', 'thema_deptos'), $widget_ops);
	}

	function form($instance)
	{
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function widget($args, $instance)
	{
		include 'Noticias4RecentesWidget.template.php';
	}
}

add_action('widgets_init', array('Noticias4RecentesWidget', 'Init'));
