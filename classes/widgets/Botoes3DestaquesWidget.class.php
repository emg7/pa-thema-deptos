<?php
class Botoes3DestaquesWidget extends WP_Widget
{
	static function Init()
	{
		register_widget(__CLASS__);
	}

	function __construct()
	{
		$widget_ops = array('classname' => __CLASS__, 'description' => __('Apresenta 3 botões com destaque nivel 2', 'thema_deptos'));
		$this->WP_Widget(__CLASS__, __('3 Botões de Destaque', 'thema_deptos'), $widget_ops);
	}

	function form($instance)
	{
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function widget($args, $instance)
	{
		include 'Botoes3DestaquesWidget.template.php';
	}
}

add_action('widgets_init', array('Botoes3DestaquesWidget', 'Init'));
