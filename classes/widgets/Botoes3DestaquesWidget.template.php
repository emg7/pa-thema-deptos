<div class="departamentos-projetos">
	<ul class="unstyled">
		<?php $projetos = new WP_Query (array ('post_type' => 'any', 'posts_per_page'=> 3, 'orderby'=> 'date', 'meta_key' => 'all_highlight', 'meta_value' => '2')); ?>
		<?php while($projetos->have_posts()): $projetos->the_post();?>
			<li class="projetos-departamentos-buttons">
				<a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
					<h3><?php echo MasterThemeController::theTitleMaxCharlength(get_the_title(), 60); ?></h3>
				</a>
			</li>
		<?php endwhile; ?>
	</ul>
</div>
<?php wp_reset_query(); ?>