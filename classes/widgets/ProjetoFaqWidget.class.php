<?php
class ProjetoFaqWidget extends WP_Widget
{

	static function Init()
	{
		register_widget(__CLASS__);
	}

	function __construct()
	{
		$widget_ops = array('classname' => __CLASS__, 'description' => __('Lista até 5 faqs do projeto', 'thema_deptos'));
		$this->WP_Widget(__CLASS__, __('Projeto: FAQ', 'thema_deptos'), $widget_ops);
	}

	function form($instance)
	{
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function widget($args, $instance)
	{
?>
		<div class="projetos-faq">
			<h2 id="faq"><?php _e('Faq', 'thema_deptos');?></h2>
			<span><?php _e('Veja aqui as dúvidas mais frequentes sobre o projeto.', 'thema_deptos');?></span>
			<dl>
				<?php
				global $post;
				$faq_query = new WP_Query (
					array(
						'post_type' => 'qa_faqs',
						'orderby' => 'date',
						'tax_query' => array(
							array(
								'taxonomy' => 'faq_category',
								'field' => 'id',
								'terms' => get_post_meta($post->ID, 'project-options-faq', true),
								'include_children' => false
							)
						)
					)
				);
				$cont = 0;
				while ($faq_query->have_posts()) : $faq_query->the_post();
					?>

					<dt class="faq-question"><span><? echo $cont+1; ?>.</span><?php echo the_title(); ?></dt>
					<dd class="faq-answer"><?php echo the_content(); ?></dd>

					<?php
					if (++$cont == 5) {
						echo "</dl><dl class='hidden-faq'>";
					}

				endwhile; wp_reset_query();
				?>
			</dl>
			<a href="#" class="more-faq more-button pull-right"><?php _e('Veja mais', 'thema_deptos');?></a>
		</div>
<?php
	}
}

add_action('widgets_init', array('ProjetoFaqWidget', 'Init'));
