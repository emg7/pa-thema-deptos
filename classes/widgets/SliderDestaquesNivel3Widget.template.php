<?php global $wp_query; ?>
<div class="departamentos-destaques">
	<div class="flexslider">
		<ul class="slides">
<?php 
	$wp_query = new WP_Query (array ('post_type'=>'any', 'posts_per_page'=> 8, 'orderby'=> 'date', 'meta_key' => 'all_highlight', 'meta_value' => '3'));
	while(have_posts()): the_post();?>
			<li>
				<div>
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('thumb_620x270'); ?>
						<div class="destaques-item">
							<h4><?php echo the_title(); ?></h4>
							<?php //<p><?php echo (function_exists('get_the_excerpt_x')) ? get_the_excerpt_x(180) : get_the_excerpt(); </p> ?>
						</div>
					</a>
				</div>
			</li>
<?php endwhile; ?>
		</ul>
	</div>
	<ol class="flex-control-nav"></ol>
</div>
<?php wp_reset_query(); ?>