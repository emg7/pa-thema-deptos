<div class="ministerio-jovem-news">
	<h2><?php _e('Últimas do blog', 'thema_deptos');?></h2>
	<div class="row-fluid">
	<?php $latest_news_query = new WP_Query (array ('type'=>'post', 'posts_per_page'=> 4, 'orderby'=> 'date')); ?>
		<div class="span6">
<?php
				if(isset($latest_news_query->posts[0])):
					global $post;
					$post = $latest_news_query->posts[0];
					the_post();
?>
			<div class="n-pic2-hor">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<?php the_post_thumbnail('thumb_140x90'); ?>
				<p><?php echo (function_exists('get_the_excerpt_x')) ? get_the_excerpt_x() : get_the_excerpt(); ?></p>
			</div>
<?php
				endif;
				if(isset($latest_news_query->posts[2])):
					global $post;
					$post = $latest_news_query->posts[2];
					the_post();
?>
			<div class="n-pic2-hor">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<?php the_post_thumbnail('thumb_140x90'); ?>
				<p><?php echo (function_exists('get_the_excerpt_x')) ? get_the_excerpt_x() : get_the_excerpt(); ?></p>
			</div>
<?php endif; ?>
		</div>
		<div class="span6">
			<?php
				if(isset($latest_news_query->posts[1])):
					global $post;
					$post = $latest_news_query->posts[1];
					the_post();
			?>
			<div class="n-pic2-hor">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<?php the_post_thumbnail('thumb_140x90'); ?>
				<p><?php echo (function_exists('get_the_excerpt_x')) ? get_the_excerpt_x() : get_the_excerpt(); ?></p>
			</div>
			<?php
				endif;
				if(isset($latest_news_query->posts[3])):
					global $post;
					$post = $latest_news_query->posts[3];
					the_post();
			?>
			<div class="n-pic2-hor">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<?php the_post_thumbnail('thumb_140x90'); ?>
				<p><?php echo (function_exists('get_the_excerpt_x')) ? get_the_excerpt_x() : get_the_excerpt(); ?></p>
			</div>
<?php endif; ?>
		</div>
	</div>
	<a class="more-button pull-right" href="#"><?php _e('Veja mais no blog', 'thema_deptos');?></a>
</div>
<?php wp_reset_query(); ?>