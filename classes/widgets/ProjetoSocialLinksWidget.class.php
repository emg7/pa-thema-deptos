<?php
class ProjetoSocialLinksWidget extends WP_Widget
{
	static function Init()
	{
		register_widget(__CLASS__);
	}

	function __construct()
	{
		$widget_ops = array('classname' => __CLASS__, 'description' => __('Facebook, twitter, RSS do Projeto', 'thema_deptos'));
		$this->WP_Widget(__CLASS__, __('Projeto: Links Sociais', 'thema_deptos'), $widget_ops);
	}

	function form($instance)
	{
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function widget($args, $instance)
	{
		global $post;

		$url = get_post_meta($post->ID, 'project-options-hotsite', true);
		$twitter = get_post_meta($post->ID, 'project-options-twitter', true);
		$facebook = get_post_meta($post->ID, 'project-options-facebook', true);
		$rss = get_post_meta($post->ID, 'project-options-rss', true);
		
		if ($url or $twitter or $facebook or $rss) {

?>
		<div class="projetos-encontre">
			<h2 id="encontre-nos"><?php _e('Encontre-nos', 'thema_deptos');?></h2>
			<?php if ($url) { ?><a class="hotsite" href="<?php echo $url; ?>"><?php echo $url; ?></a><?php } ?>
			<ul class="social-networks unstyled">
				<?php if ($facebook) { ?><li><a href="http://www.facebook.com/<?php echo $facebook; ?>" target="_blank"><span class="projetos iasd-icon facebook"></span></a></li> <?php } ?>
				<?php if ($twitter) { ?><li><a href="http://twitter.com/<?php echo $twitter; ?>" target="_blank"><span class="projetos iasd-icon twitter"></span></a></li> <?php } ?>
				<?php if ($rss) { ?><li><a href="<?php echo $rss; ?>"><span class="projetos iasd-icon rss" target="_blank"></span></a></li> <?php } ?>
			</ul>
		</div>
<?php

		} //endif
	}
}

add_action('widgets_init', array('ProjetoSocialLinksWidget', 'Init'));
