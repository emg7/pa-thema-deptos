<?php
class CustomBotoes3DestaquesWidget extends WP_Widget
{
	static function Init()
	{
		register_widget(__CLASS__);
	}

	function CustomBotoes3DestaquesWidget() {
		$widget_ops = array( 'classname' => 'custombuttons', 'description' => __('Widget que mostra 3 botoes ao lado do slider na página inicial do site', 'thema_deptos') );
		
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'custombuttons-widget' );
		
		$this->WP_Widget( 'custombuttons-widget', __('3 Botões de Destaque - Custom', 'thema_deptos'), $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );

		//Our variables from the widget settings.
		$titleA = apply_filters('widget_title', $instance['titleA'] );
		$linkA = $instance['linkA'];

		$titleB = apply_filters('widget_title', $instance['titleB'] );
		$linkB = $instance['linkB'];

		$titleC = apply_filters('widget_title', $instance['titleC'] );
		$linkC = $instance['linkC'];

		echo $before_widget;

		?>

		<ul class="unstyled">
			<?php if ( $titleA ) { ?>
			<li class="projetos-departamentos-buttons">
				<a href="<?php echo $linkA ?>" title="<?php echo $titleA ?>">
					<h3><?php echo $titleA ?></h3>
				</a>
			</li>
			<?php } ?>
			<?php if ( $titleB ) { ?>
			<li class="projetos-departamentos-buttons">
				<a href="<?php echo $linkB ?>" title="<?php echo $titleB ?>">
					<h3><?php echo $titleB ?></h3>
				</a>
			</li>
			<?php } ?>
			<?php if ( $titleC ) { ?>
			<li class="projetos-departamentos-buttons">
				<a href="<?php echo $linkC ?>" title="<?php echo $titleC ?>">
					<h3><?php echo $titleC ?></h3>
				</a>
			</li>
			<?php } ?>
		</ul>

		<?php

		
		echo $after_widget;
	}
	 
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['titleA'] = strip_tags( $new_instance['titleA'] );
		$instance['linkA'] = strip_tags( $new_instance['linkA'] );

		$instance['titleB'] = strip_tags( $new_instance['titleB'] );
		$instance['linkB'] = strip_tags( $new_instance['linkB'] );

		$instance['titleC'] = strip_tags( $new_instance['titleC'] );
		$instance['linkC'] = strip_tags( $new_instance['linkC'] );

		return $instance;
	}

	
	function form( $instance ) {
		/*
		$defaults = array( 'titleA' => __('Example', 'thema_deptos'), 'linkA' => __('Bilal Shaheen', 'thema_deptos'), 
			'titleB' => __('Example', 'thema_deptos'), 'linkB' => __('Bilal Shaheen', 'thema_deptos'), 
			'titleC' => __('Example', 'thema_deptos'), 'linkC' => __('Bilal Shaheen', 'thema_deptos') );
		*/
		$instance = wp_parse_args( (array) $instance, $defaults );

		include 'CustomBotoes3DestaquesWidget.template.php';
	}
}

add_action('widgets_init', array('CustomBotoes3DestaquesWidget', 'Init'));
