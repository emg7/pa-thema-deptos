<?php
class TestemunhosLatestWidget extends WP_Widget
{

	static function Init()
	{
		register_widget(__CLASS__);
	}

	function __construct()
	{
		$widget_ops = array('classname' => __CLASS__, 'description' => '');
		$this->WP_Widget(__CLASS__, __('Testemunhos mais recentes', 'thema_deptos'), $widget_ops);
	}

	function form($instance)
	{
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function widget($args, $instance)
	{
		global $post;
		$query = new WP_Query (
			array(
				'post_type' => TestemunhosController::$post_type_name,
				'posts_per_page' => 3,
				'meta_key' => 'testemunhos_highlight',
				'meta_value' => '1'
			)
		);
?>
		<div class="departamentos-testemunhos">
			<h2 id="testemunho"><?php _e('Testemunhos', 'thema_deptos'); ?></h2>
			<?php  if($query->have_posts()): $query->the_post();?>
			<div class="departamentos-testemunhos-highlight">
				<?php echo get_the_post_thumbnail($post->ID,'thumb_300x170') ?>
				<a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a>
			</div>
			<?php endif; ?>
			<ul class="unstyled departamentos-testemunhos-inner">
<?php
				wp_reset_query();
				$query = new WP_Query (
					array(
						'post_type' => TestemunhosController::$post_type_name,
						'posts_per_page' => 3
					)
				);
				$cont = 0;
				while ($query->have_posts()) : $query->the_post();
					$format = get_post_format();
					if($format == 'image')
						$format = 'foto';
					else if($format == 'video')
							$format = 'video';
					else if($format == 'audio')
							$format = 'audio';
					else
						$format = 'documento';
?>
					<li>
						<a href="<?php echo the_permalink(); ?>"><span class="iasd-icon <?php echo $format; ?>"></span><?php echo the_title(); ?></a>
					</li>
<?php
				endwhile; wp_reset_query();
?>
			</ul>
			<a class="more-button" href="<?php echo get_post_type_archive_link(TestemunhosController::$post_type_name); ?>"><?php _e('Veja mais testemunhos', 'thema_deptos');?></a>
		</div>
<?php
	}
}

add_action('widgets_init', array('TestemunhosLatestWidget', 'Init'));
