<?php
class SliderDestaquesNivel3Widget extends WP_Widget
{
	static function Init()
	{
		register_widget(__CLASS__);
	}

	function __construct()
	{
		$widget_ops = array('classname' => __CLASS__, 'description' => __('Apresenta as 4 destaques nivel 3 mais recentes', 'thema_deptos'));
		$this->WP_Widget(__CLASS__, __('Slider com 4 Destaques Nivel 3', 'thema_deptos'), $widget_ops);
	}

	function form($instance)
	{
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function widget($args, $instance)
	{
		include 'SliderDestaquesNivel3Widget.template.php';
	}
}

add_action('widgets_init', array('SliderDestaquesNivel3Widget', 'Init'));
