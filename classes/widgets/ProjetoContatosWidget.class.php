<?php
class ProjetoContatosWidget extends WP_Widget
{
	static function Init()
	{
		register_widget(__CLASS__);
	}

	function __construct()
	{
		$widget_ops = array('classname' => __CLASS__, 'description' => __('Lista os contatos associados ao projeto do departamento', 'thema_deptos'));
		$this->WP_Widget(__CLASS__, __('Projeto: Contatos', 'thema_deptos'), $widget_ops);
	}

	function form($instance)
	{
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function widget($args, $instance)
	{
		global $post;
?>
	<div class="projetos-contato">
		<h2 id="contatos"><?php _e('Contato', 'thema_deptos');?></h2>
		<p><?php _e('Ou entre em contato diretamente com nossa equipe.', 'thema_deptos'); ?></p>
		<div class="row-fluid">
			<?php
			global $post;
			$contatos_query = new WP_Query (
			array(
				'post_type'=>'contatos',
				'orderby'=> 'date',
				'posts_per_page'=> 6,
				'tax_query' => array(
					array(
						'taxonomy' => 'grupos_contatos',
						'field' => 'id',
						'terms' => get_post_meta($post->ID, 'project-options-contatos', true),
						'include_children' => false
						)
					)
				)
			);
			$i = 1;
			while( $contatos_query->have_posts() ) : $contatos_query->the_post();
			?>
			<div class="span6 item-contato">
				<?php
				$contatos_cargo = get_post_meta($post->ID, 'info-contatos-cargo', true);
				$contatos_email = get_post_meta($post->ID, 'info-contatos-email', true);
				?>
				<div class="row-fluid">
					<?php 
					$texto_span = 'span12';
					if (has_post_thumbnail( )): 
						$texto_span = 'span9'; 
					?>
					<div class="span3"><?php echo the_post_thumbnail( 'thumb_60x60' ); ?></div>
				<?php endif; ?>
					<div class="<?php echo $texto_span; ?>" >
						<strong><?php echo the_title(); ?></strong>
						<span><?php echo $contatos_cargo; ?></span>
						<a href="mailto:<?php echo $contatos_email; ?>"><?php echo $contatos_email; ?></a>
					</div>
				</div>
			</div>
			<?php

			if($i % 2 == 0) { echo '</div><div class="row-fluid">';}
			$i++;
			endwhile;
			wp_reset_query();
			?>
		</div>
	</div>
<?php
	}
}

add_action('widgets_init', array('ProjetoContatosWidget', 'Init'));
