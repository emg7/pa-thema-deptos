<?php
class NossosProjetosWidget extends WP_Widget
{

	static function Init()
	{
		register_widget(__CLASS__);
	}

	function __construct()
	{
		$widget_ops = array('classname' => __CLASS__, 'description' => __('Lista os projetos do departamento', 'thema_deptos'));
		$this->WP_Widget(__CLASS__, __('Projetos do Departamento', 'thema_deptos'), $widget_ops);
	}

	function form($instance)
	{
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function widget($args, $instance)
	{
		$args = array('post_type' => 'projetos', 'orderby' => 'rand');
		$projetos = new WP_Query($args);
?>

		<h2><?php _e('Nossos Projetos', 'thema_deptos');?></h2>
		<div class="departamentos-nossos-projetos">
			<ul class="unstyled">
<?php	while($projetos->have_posts()): $projetos->the_post(); ?>
				<li>
					<a href="<?php the_permalink(); ?>"><b><?php the_title(); ?></b><br/><?php echo get_post_meta(get_the_ID(), 'project-options-url', true); ?></a>
				</li>
<?php endwhile; ?>
			</ul>
			<a class="see-more" href="<?php echo get_post_type_archive_link('projetos'); ?>"><?php _e('Veja mais projetos', 'thema_deptos');?></a>
		</div>
<?php
		wp_reset_query();
	}
}

add_action('widgets_init', array('NossosProjetosWidget', 'Init'));

