<fieldset style="margin-bottom: 20px;">
	<h3>Botão 1</h3>
	<label for="<?php echo $this->get_field_id( 'titleA' ); ?>"><?php _e('Título do botão:', 'thema_deptos'); ?></label>
	<input id="<?php echo $this->get_field_id( 'titleA' ); ?>" name="<?php echo $this->get_field_name( 'titleA' ); ?>" value="<?php echo $instance['titleA']; ?>" style="width:96%;" />
	<br />
	<label for="<?php echo $this->get_field_id( 'linkA' ); ?>"><?php _e('Link de destino:', 'thema_deptos'); ?></label>
	<input id="<?php echo $this->get_field_id( 'linkA' ); ?>" name="<?php echo $this->get_field_name( 'linkA' ); ?>" value="<?php echo $instance['linkA']; ?>" style="width:96%;" />
</fieldset>

<fieldset style="margin-bottom: 20px;">
	<h3>Botão 2</h3>
	<label for="<?php echo $this->get_field_id( 'titleB' ); ?>"><?php _e('Título do botão:', 'thema_deptos'); ?></label>
	<input id="<?php echo $this->get_field_id( 'titleB' ); ?>" name="<?php echo $this->get_field_name( 'titleB' ); ?>" value="<?php echo $instance['titleB']; ?>" style="width:96%;" />
	<br />
	<label for="<?php echo $this->get_field_id( 'linkB' ); ?>"><?php _e('Link de destino:', 'thema_deptos'); ?></label>
	<input id="<?php echo $this->get_field_id( 'linkB' ); ?>" name="<?php echo $this->get_field_name( 'linkB' ); ?>" value="<?php echo $instance['linkB']; ?>" style="width:96%;" />
</fieldset>

<fieldset style="margin-bottom: 10px;">
	<h3>Botão 3</h3>
	<label for="<?php echo $this->get_field_id( 'titleC' ); ?>"><?php _e('Título do botão:', 'thema_deptos'); ?></label>
	<input id="<?php echo $this->get_field_id( 'titleC' ); ?>" name="<?php echo $this->get_field_name( 'titleC' ); ?>" value="<?php echo $instance['titleC']; ?>" style="width:96%;" />
	<br />
	<label for="<?php echo $this->get_field_id( 'linkC' ); ?>"><?php _e('Link de destino:', 'thema_deptos'); ?></label>
	<input id="<?php echo $this->get_field_id( 'linkC' ); ?>" name="<?php echo $this->get_field_name( 'linkC' ); ?>" value="<?php echo $instance['linkC']; ?>" style="width:96%;" />
</fieldset>
