<?php
class NossosArtigosWidget extends WP_Widget
{

	static function Init()
	{
		register_widget(__CLASS__);
	}

	function __construct()
	{
		$widget_ops = array('classname' => __CLASS__, 'description' => __('Lista alguns posts do departamento', 'thema_deptos'));
		$this->WP_Widget(__CLASS__, __('Artigos do Departamento', 'thema_deptos'), $widget_ops);
	}

	function form($instance)
	{
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function widget($args, $instance)
	{
		$args = array('post_type' => 'post', 'posts_per_page'=> 5, 'orderby' => 'rand');
		$projetos = new WP_Query($args);
?>

		<h2><?php _e('Nossos Artigos', 'thema_deptos'); ?></h2>
		<div class="departamentos-list-artigos">
			<ul class="unstyled">
<?php	while($projetos->have_posts()): $projetos->the_post(); ?>
				<li>
					<a href="<?php the_permalink(); ?>">
						<span class="icone-arquivo"></span>
						<div class="informacoes">
								<span class="titulo">
									<?php echo (class_exists('TextManipulation')) ? TextManipulation::TrimChars(get_the_title(), 33) : get_the_title(); ?>
								</span>
								<span class="autor"><?php the_author(); ?></span>
								<span class="data"><?php the_time('j F Y'); ?></span>
						</div>
					</a>
				</li>
<?php endwhile; ?>
			</ul>
			<div class="clear"></div>
			<a class="see-more" href="<?php echo get_site_url(); ?>/blog/"><?php _e('Veja mais artigos', 'thema_deptos'); ?></a>
		</div>
<?php
		wp_reset_query();
	}
}

add_action('widgets_init', array('NossosArtigosWidget', 'Init'));

