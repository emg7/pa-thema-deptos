<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sidferreira
 * Date: 31/01/2013
 * Time: 11:03
 * To change this template use File | Settings | File Templates.
 */

class ProjetosController {

    public static function Init() {

        self::CreatePostType();

		self::RegisterSidebar();

    }

	public static function RegisterSidebar() {
		$sidebars = array();
		$sidebars[] = array('name' => __('Linha 1', 'thema_deptos'),
				'id' => 'row-1',
				'description' => __('Espaço dos projetos, na 1a linha', 'thema_deptos'));
		$sidebars[] = array('name' => __('Linha 2 Esquerda', 'thema_deptos'),
				'id' => 'row-2-left',
				'description' => __('Espaço dos projetos, à esquerda, na 2a linha', 'thema_deptos'));
		$sidebars[] = array('name' => __('Linha 2 Direita', 'thema_deptos'),
				'id' => 'row-2-right',
				'description' => __('Espaço dos projetos, à direita, na 2a linha', 'thema_deptos'));
		$sidebars[] = array('name' => __('Linha 3 Esquerda', 'thema_deptos'),
				'id' => 'row-3-left',
				'description' => __('Espaço dos projetos, à esquerda, na 3a linha', 'thema_deptos'));
		$sidebars[] = array('name' => __('Linha 3 Direita', 'thema_deptos'),
				'id' => 'row-3-right',
				'description' => __('Espaço dos projetos, à direita, na 3a linha', 'thema_deptos'));
		$sidebars[] = array('name' => __('Linha 4', 'thema_deptos'),
				'id' => 'row-4',
				'description' => __('Espaço dos projetos, na 4a linha', 'thema_deptos'));
		$sidebars[] = array('name' => __('Linha 5', 'thema_deptos'),
				'id' => 'row-5',
				'description' => __('Espaço dos projetos, na 5a linha', 'thema_deptos'));
		$sidebars[] = array('name' => __('Linha 6', 'thema_deptos'),
				'id' => 'row-6',
				'description' => __('Espaço dos projetos, na 6a linha', 'thema_deptos'));
		$sidebars[] = array('name' => __('Sidebar', 'thema_deptos'),
				'id' => 'sidebar',
				'description' => __('Sidebar à esquerda, abaixo da imagem', 'thema_deptos'));

		if ( function_exists('register_sidebar') ) {
			foreach ($sidebars as $sidebar) {
				register_sidebar(array(
					'name' => __('Projetos: ', 'thema_deptos') . $sidebar['name'],
					'id' => 'projetos-'.$sidebar['id'],
					'description' => $sidebar['description'],
					'before_widget' => '<div class="widget">',
					'after_widget' => '</div>',
					'before_title' => '<h2>',
					'after_title' => '</h2>',
				));
			}
			$projetos = query_posts( array('post_type' => 'projetos') );
			foreach($projetos as $projeto) {
				foreach ($sidebars as $sidebar) {
					register_sidebar(array(
						'name' => __('Projetos: ', 'thema_deptos') . $projeto->post_title . ': ' . $sidebar['name'],
						'id' => 'projetos-'.$projeto->post_name.'-'.$sidebar['id'],
						'description' => $sidebar['description'],
						'before_widget' => '<div class="widget">',
						'after_widget' => '</div>',
						'before_title' => '<h2>',
						'after_title' => '</h2>',
					));
				}
			}
			wp_reset_query();
		}

	}

	public static function AdminMenu() {
		register_setting('general', 'asf_projeto_permalink_single');
		register_setting('general', 'asf_projeto_permalink_archive');
		add_settings_section('ass_permalinks_projetos', __('Projetos', 'thema_depto'), array('ProjetosController', 'ASSPermalinks'), 'general');
		add_settings_field('asf_projeto_permalink_single', __('Single', 'thema_depto'), array('ProjetosController', 'ASFPermalinks'), 'general', 'ass_permalinks_projetos', 'single');
		add_settings_field('asf_projeto_permalink_archive', __('Archive', 'thema_depto'), array('ProjetosController', 'ASFPermalinks'), 'general', 'ass_permalinks_projetos', 'archive');
	}
	public static function ASSPermalinks() {
		echo '<p>' . __('Use os campos abaixo para configurar os permalinks relacionados aos projetos dos departamentos', 'thema_depto') . '</p>';
	}
	public static function ASFPermalinks($type) {
		switch ($type) {
			case 'single':
				echo '<input name="asf_projeto_permalink_single" id="asf_projeto_permalink_single" type="input" value="'. self::SingleSlug() .'" class="code"  />';
				break;
			case 'archive':
				echo '<input name="asf_projeto_permalink_archive" id="asf_projeto_permalink_archive" type="input" value="'. self::ArchiveSlug() .'" class="code"  />';
				break;
		}
	}
	public static function SingleSlug() {
		return get_option('asf_projeto_permalink_single', 'projeto');
	}
	public static function ArchiveSlug() {
		return get_option('asf_projeto_permalink_archive', 'projetos');
	}

    public static function CreatePostType() {
        $labels = array(
            'name' => __('Projetos', 'thema_deptos'),
            'singular_name' => __('Projetos', 'thema_deptos'),
            'add_new' => __('Adicionar novo', 'thema_deptos'),
            'add_new_item' => __('Adicionar novo projetos', 'thema_deptos'),
            'edit_item' => __('Editar projetos', 'thema_deptos'),
            'new_item' => __('Novo projetos', 'thema_deptos'),
            'view_item' => __('Visualizar projetos', 'thema_deptos'),
            'search_items' => __('Buscar projetos', 'thema_deptos')
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            //'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
            'rewrite' => array('slug' => self::SingleSlug()),
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array('title','editor','thumbnail'),
			'has_archive' => self::ArchiveSlug()
        );

        register_post_type( 'projetos' , $args );
    }

    public static function TaxonomiesContatos() {
        $base_list = get_categories(array('taxonomy' => 'grupos_contatos'));
        $list = array();

        foreach($base_list as $v)
            $list[$v->term_id] = $v;

        return $list;
    }

    public static function TaxonomiesGalerias() {
        $base_list = get_categories(array('taxonomy' => PAImageGallery::$taxonomy_name));
        $list = array();

        foreach($base_list as $v)
            $list[$v->term_id] = $v;

        return $list;
    }

    public static function TaxonomiesFAQs() {
        $base_list = get_categories(array('taxonomy' => 'faq_category'));
        $list = array();

        foreach($base_list as $v)
            $list[$v->term_id] = $v;

        return $list;
    }

	public static function TaxonomiesNews() {
		return get_option('asn_noticias_category_list', array());
	}

    //Social Meta Bozes

    public static function MetaboxInfo() {
        return array(
            'id' => 'project-options',
            'title' => __('Opções de Exibição', 'thema_deptos'),
            'post_type' => 'projetos',
            'context' => 'normal',
            'priority' => 'low',
            'fields' => array(
                array(
                    'name' => __('Contatos', 'thema_deptos'),
                    'id' => 'project-options-contatos',
                    'type' => 'select',
                    'std' => '',
                    'method' => array('ProjetosController', 'TaxonomiesContatos')
                ),
                array(
                    'name' => __('Galerias', 'thema_deptos'),
                    'id' => 'project-options-galerias',
                    'type' => 'select',
                    'std' => '',
                    'method' => array('ProjetosController', 'TaxonomiesGalerias')
                ),
                array(
                    'name' => __('FAQs', 'thema_deptos'),
                    'id' => 'project-options-faq',
                    'type' => 'select',
                    'std' => '',
                    'method' => array('ProjetosController', 'TaxonomiesFAQs')
                ),
				array(
					'name' => __('Notícias', 'thema_deptos'),
					'id' => 'project-options-news',
					'type' => 'select',
					'std' => '',
					'method' => array('ProjetosController', 'TaxonomiesNews')
				),
				array(
					'name' => __('RSS', 'thema_deptos'),
					'id' => 'project-options-rss',
					'type' => 'text',
					'std' => '',
				),
				array(
					'name' => __('Twitter', 'thema_deptos'),
					'id' => 'project-options-twitter',
					'type' => 'text',
					'std' => '',
				),
				array(
					'name' => __('Facebook', 'thema_deptos'),
					'id' => 'project-options-facebook',
					'type' => 'text',
					'std' => '',
				),
				array(
					'name' => __('Endereço', 'thema_deptos'),
					'id' => 'project-options-url',
					'type' => 'text',
					'std' => '',
				),
				array(
					'name' => __('Hotsite', 'thema_deptos'),
					'id' => 'project-options-hotsite',
					'type' => 'text',
					'std' => '',
				),
            )
        );
    }
    // Add meta box
    public static function OptionsMetaBox() {
        $meta_box = self::MetaboxInfo();

        add_meta_box($meta_box['id'], $meta_box['title'], array('ProjetosController', 'OptionsMetaBoxShow'), $meta_box['post_type'], $meta_box['context'], $meta_box['priority']);
    }

    // Callback function to show fields in meta box
    public static function OptionsMetaBoxShow() {
        global $post;
        $meta_box = self::MetaboxInfo();

        // Use nonce for verification
        echo '<input type="hidden" name="options_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

        echo '<table class="form-table">';

		echo '<tr>';

        foreach ($meta_box['fields'] as $field) {
            // get current post meta data
            $meta = get_post_meta($post->ID, $field['id'], true);

            echo '<th style="width:20%">';
			echo '<label for="', $field['id'], '">', $field['name'], '</label>';
    		echo '</th><td>';

			if($field['type'] == 'select') {
				$method = $field['method'];
				if(is_array($method))
					$method = implode('::', $method);

				$options = call_user_func($method);

				echo '<select name="', $field['id'], '" id="', $field['id'], '" style="width:97%" >';
				echo '<option value="" ', (!$meta) ? 'selected="selected"' : '', '> --- </option>';
				foreach($options as $id => $option)
					echo '<option value="', $id, '" ', ($meta == $id) ? 'selected="selected"' : '', '>', (is_object($option) ? $option->name : $option), '</option>';
				echo '</select>';
			} else if($field['type'] == 'text') {

				echo '<input name="', $field['id'], '" id="', $field['id'], '" value="', get_post_meta($post->ID, $field['id'], 1), '" class="large-text" />';

			}

			echo '<br /><td></tr>';
        }


        echo '</table>';
    }

    // Save data from meta box
    public static function OptionsMetaBoxSave($post_id) {
        if ( wp_is_post_autosave($post_id) || wp_is_post_revision($post_id) || !isset($_POST['options_meta_box_nonce']))
            return false;

        $meta_box = self::MetaBoxInfo();

        // verify nonce
        $nounce = (isset($_POST['options_meta_box_nonce'])) ? $_POST['options_meta_box_nonce'] : '' ;
        if (!wp_verify_nonce($nounce, basename(__FILE__))) {
            return $post_id;
        }

        // check autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $post_id;
        }

        // check permissions
        if ('page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id)) {
                return $post_id;
            }
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        foreach ($meta_box['fields'] as $field) {
            $new = (isset($_POST[$field['id']])) ? $_POST[$field['id']] : false;
            update_post_meta($post_id, $field['id'], $new);
        }
    }
	public static function Breadcrumber($struct, $type_object = null) {
		global $wp_query, $wp;
		$initial_wp_query = $wp_query;
		$object = get_queried_object();

		$query_info = array();
		parse_str($wp->matched_query, $query_info);
		$post_type_name = $wp_query->query_vars['post_type'];

		if($post_type_name == PAImageGallery::$post_type_name && isset($query_info['projeto'])) {
			$projeto = reset(query_posts(array('name' => $query_info['projeto'], 'post_type' => 'projetos')));
			$post_type = get_post_type_object( 'projetos' );

			$struct['sub1'] = $struct['type'];
			$struct['subt'] = array('name' => get_the_title($projeto->ID), 'link' => get_permalink($projeto->ID));
			$struct['type'] = array('name' => $post_type->label, 'link' => get_post_type_archive_link( 'projetos' ));
			
			$wp_query = $initial_wp_query;
		}

		return $struct;
	}
}
add_filter('breadcrumber_struct', array('ProjetosController', 'Breadcrumber'), 10, 2);

add_action('init', array('ProjetosController', 'Init'));

add_action('admin_menu', array('ProjetosController', 'OptionsMetaBox') );
add_action('admin_menu', array('ProjetosController', 'AdminMenu') );

add_action('save_post', array('ProjetosController', 'OptionsMetaBoxSave') );