<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sidferreira
 * Date: 31/01/2013
 * Time: 11:03
 * To change this template use File | Settings | File Templates.
 */

class LideresController {

    public static function Init() {

        self::CreatePostType();

        //self::RegisterTaxonomies();

        self::ThumbSize();

	}

    public static function ThumbSize() {
        if ( function_exists( 'add_image_size' ) ) {
            add_image_size( 'lideres_image', 140, 140, true ); //(cropped)
        }
    }

    public static function CreatePostType() {
        $labels = array(
            'name' => __('Líderes', 'thema_deptos'),
            'singular_name' => __('Líder', 'thema_deptos'),
            'add_new' => __('Adicionar novo', 'thema_deptos'),
            'add_new_item' => __('Adicionar novo Liderança', 'thema_deptos'),
            'edit_item' => __('Editar Líder', 'thema_deptos'),
            'new_item' => __('Novo Líder', 'thema_deptos'),
            'view_item' => __('Visualizar Líder', 'thema_deptos'),
            'search_items' => __('Buscar Líder', 'thema_deptos')
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            //'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
            'rewrite' => array('slug' => 'lider'),
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array('title','editor','thumbnail','excerpt'),
			'has_archive' => 'lideres'
        );

        register_post_type( 'lideres' , $args );
//		add_filter('registra_taxonomia_sedes_regionais_post_type', array(__CLASS__, 'TaxonomiaSedesRegionais'));
	}

	public static function TaxonomiaSedesRegionais($args) {
		$args[] = 'lideres';
		return $args;
    }

/*    public static function RegisterTaxonomies() {

        //Register categories for the custom type
        register_taxonomy( 'conference', 'lideres', array(
                'hierarchical' => true,
                'label' => __( 'Categoria Uniões' ),
                'show_ui' => true
            )
        );
		add_filter('wp_terms_checklist_args', array('LideresController', 'ChecklistArgs'));
    }*/

	public static function ChecklistArgs($args) {
		if($args['taxonomy'] == 'conference')
			$args['walker'] = new Walker_Category_Radiolist();

		return $args;
	}

    //Social Meta Bozes

    public static function MetaBoxInfo() {
        return array(
            'id' => 'social-networks',
            'title' => __('Informações Adicioniais', 'thema_deptos'),
            'post_type' => 'lideres',
            'context' => 'side',
            'priority' => 'high',
            'fields' => array(
                array(
                    'name' => __('E-mail', 'thema_deptos'),
                    'id' => 'social-networks_email',
                    'type' => 'text',
                    'std' => ''
                ),
                array(
                    'name' => __('Twitter', 'thema_deptos'),
                    'id' => 'social-networks_twitter',
                    'type' => 'text',
                    'std' => ''
                ),
                array(
                    'name' => __('Facebook', 'thema_deptos'),
                    'id' => 'social-networks_facebook',
                    'type' => 'text',
                    'std' => ''
                ),
                array(
                    'name' => __('Cargo', 'thema_deptos'),
                    'id' => 'iasd_cargo',
                    'type' => 'text',
                    'std' => ''
                ),
                array(
                    'name' => __('Líder Geral', 'thema_deptos'),
                    'id' => 'iasd_lider',
                    'type' => 'checkbox',
                    'std' => ''
                ),
            )
        );
    }
    // Add meta box
    public static function SocialMetaBox() {
        $meta_box = self::MetaBoxInfo();

        add_meta_box($meta_box['id'], $meta_box['title'], array('LideresController', 'SocialMetaBoxShow'), $meta_box['post_type'], $meta_box['context'], $meta_box['priority']);
    }

    // Callback function to show fields in meta box
    public static function SocialMetaBoxShow() {
        global $post;
        $meta_box = self::MetaBoxInfo();

        // Use nonce for verification
        echo '<input type="hidden" name="social_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

        echo '<table class="form-table">';

        foreach ($meta_box['fields'] as $field) {
            // get current post meta data
            $meta = get_post_meta($post->ID, $field['id'], true);

            echo '<tr>',
            '<th style="width:20%"><label for="', $field['id'], '">', $field['name'], '</label></th>',
            '<td>';
            switch ($field['type']) {
                case 'text':
                    echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />', '<br />';
                    break;
                case 'checkbox':
                    echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '" ', $meta ? 'checked="checked"' : '', '/><br />';
                    break;
            }
            echo     '<td>',
            '</tr>';
        }

        echo '</table>';
    }

    // Save data from meta box
    public static function SocialMetaBoxSave($post_id) {
        if ( wp_is_post_autosave($post_id) || wp_is_post_revision($post_id) || !isset($_POST['social_meta_box_nonce']))
            return false;

        $meta_box = self::MetaBoxInfo();

        // verify nonce
        $nounce = (isset($_POST['social_meta_box_nonce'])) ? $_POST['social_meta_box_nonce'] : '' ;
        if (!wp_verify_nonce($nounce, basename(__FILE__))) {
            return $post_id;
        }

        // check autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $post_id;
        }

        // check permissions
        if ('page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id)) {
                return $post_id;
            }
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        foreach ($meta_box['fields'] as $field) {
            $new = (isset($_POST[$field['id']])) ? $_POST[$field['id']] : false;
            //var_dump($new);
            //var_dump($field);

            update_post_meta($post_id, $field['id'], $new);
            //var_dump(get_post_meta($post_id, $field['id']), true);
        }
    }
}

add_action('init', array('LideresController', 'Init'));

add_action('admin_menu', array('LideresController', 'SocialMetaBox') );

add_action('save_post', array('LideresController', 'SocialMetaBoxSave') );