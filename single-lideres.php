<?php

/*
Template Name: Biografias
*/

get_header();
if(have_posts())
	the_post();

	$regiao_list = wp_get_post_terms(get_the_ID(), PATaxonomias::TAXONOMY_SEDES_REGIONAIS);

	if(is_array($regiao_list) && count($regiao_list))
		$regiao = reset($regiao_list);
	else
		$regiao = '';

	$regiao_name = ($regiao) ? $regiao->name : '';
?>

<section class="departamentos-biografias">
	<div class="container">
		<div class="row biografia">
			<?php if(function_exists('breadcrumber')) breadcrumber($regiao_name); ?>
			<div class="span4">
				<?php the_post_thumbnail('thumb_300x300'); ?>
			</div>
			<div id="lider" class="span7">
				<h1><?php echo the_title();  ?></h1>
				<strong><?php echo get_post_meta(get_the_ID(), 'iasd_cargo', true); ?></strong>

				<p><?php echo get_the_content(); ?></p>
				<ul class="unstyled">
					<?php
					if ( $social_network_email = get_post_meta(get_the_ID(), 'social-networks_email', true) ):
						?>
						<li>
							<a href="mailto:<?php echo $social_network_email; ?>" rel="tooltip" data-placement="top" data-original-title="<?php echo $social_network_email; ?>">
								<span class="email-lideres"></span>
								<?php echo $social_network_email; ?>
							</a>
						</li>
					<?php
					endif;
					if ( $social_network_twitter = get_post_meta(get_the_ID(), 'social-networks_twitter', true) ):
						?>
						<li>
							<a href="http://www.twitter.com/<?php echo $social_network_twitter; ?>" rel="tooltip" data-placement="top" data-original-title="<?php echo $social_network_twitter; ?>">
								<span class="twitter-lideres"></span>
								<?php echo $social_network_twitter; ?>
							</a>
						</li>
					<?php
					endif;
					if ( $social_network_facebook = get_post_meta(get_the_ID(), 'social-networks_facebook', true) ):
						?>
						<li>
							<a href="http://www.facebook.com/<?php echo $social_network_facebook; ?>" rel="tooltip" data-placement="top" data-original-title="<?php echo $social_network_facebook; ?>">
								<span class="facebook-lideres"></span>
								<?php echo $social_network_facebook; ?>
							</a>
						</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>