		<footer class="footer">
			<div id="footer-bg-image">
				<div class="container">
					<?php IasdFooter::Show(); ?>
					<div class="row">
						<div class="span2 footer-menu">
							<?php if(class_exists('PAMenu')) PAMenu::Show('master_footer_1'); ?>
						</div>
						<div class="span2 footer-menu">
							<?php if(class_exists('PAMenu')) PAMenu::Show('master_footer_2'); ?>
						</div>
						<div class="span2 footer-menu">
							<?php if(class_exists('PAMenu')) PAMenu::Show('master_footer_3'); ?>
						</div>
						<div class="span2 footer-menu">
<?php
								$location = 'footer_4';
								$locations = get_nav_menu_locations();
								if(isset($locations[$location]) && $locations[$location]) {
									$menu_id = $locations[$location];
									$menu = wp_get_nav_menu_object( $menu_id );
									echo '<h4>'.$menu->name.'</h4>';
									wp_nav_menu( array( 'theme_location'  => $location, 'menu_class' => 'unstyled', 'depth' => 1) );
								}
?>
						</div>
						<div class="span4 copyright-container">
							<span class="iasd-address"><?php _e('Av. L3 Sul, SGAS 611 - Conj. D, Parte C,<br/>Asa Sul - Brasília, DF - CEP 70200-710<br/>Tel. (61) 3701-1818', 'thema_asn'); ?></span>
							<h5><?php if($link = get_option('pafooter_titulo', __('Copyright © 2013 <br /> Igreja Adventista do Sétimo Dia', 'thema_deptos'))) echo $link; ?></h5>
							<p><?php if($link = get_option('pafooter_resumo', __('Todos os direitos reservados', 'thema_deptos'))) echo $link; ?></p>
							<ul class="social-networks unstyled">
								<?php if($link = get_option('pafooter_facebook')): ?><li><a target="_blank" href="<?php echo $link;; ?>"><span class="iasd-icons facebook"></span></a></li><?php endif; ?>
								<?php if($link = get_option('pafooter_twitter')): ?><li><a target="_blank" href="<?php echo $link; ?>"><span class="iasd-icons twitter"></span></a></li><?php endif; ?>
								<?php if($link = get_option('pafooter_google')): ?><li><a target="_blank" href="<?php echo $link; ?>"><span class="iasd-icons google"></span></a></li><?php endif; ?>
								<?php if($link = get_option('pafooter_youtube')): ?><li><a target="_blank" href="<?php echo $link; ?>"><span class="iasd-icons youtube"></span></a></li><?php endif; ?>
								<?php if($link = get_option('pafooter_rss')): ?><li><a href="<?php echo $link; ?>"><span class="iasd-icons rss"></span></a></li><?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
			</footer>
		</div>
		<?php wp_footer(); ?>
<script type="text/javascript">
  var GoSquared = {};
  GoSquared.acct = "GSN-569730-R";
  (function(w){
    function gs(){
      w._gstc_lt = +new Date;
      var d = document, g = d.createElement("script");
      g.type = "text/javascript";
      g.src = "//d1l6p2sc9645hc.cloudfront.net/tracker.js";
      var s = d.getElementsByTagName("script")[0];
      s.parentNode.insertBefore(g, s);
    }
    w.addEventListener ?
      w.addEventListener("load", gs, false) :
      w.attachEvent("onload", gs);
  })(window);
</script>
	</body>

</html>